#version 330 core

uniform mat4 screenMat;
uniform mat4 cameraMat;
uniform mat4 modelMat;

in vec4 in_Position;
in vec4 in_Color;
in vec2 in_TexCoord;

out vec4 pass_Color;
out vec2 pass_TexCoord;

void main(void) {
    gl_Position = screenMat * cameraMat * modelMat * in_Position;
    pass_Color = in_Color;
    pass_TexCoord = in_TexCoord;
}
