#version 330 core

uniform vec2 mouse;
uniform vec4 modColor;

out vec4 out_Color;

float random (vec2 st) {
    return fract(sin(dot(st.xy, vec2(12.9898, 78.233))) * 43758.5453123);
}

void main( void ) {
	vec2 diff = (gl_FragCoord.xy - mouse);
	vec2 relPos = (gl_FragCoord.xy - mouse * exp(-.001 * length(diff))) / 10.;
	vec2 wot = cos(relPos) + cos(2.35612 * relPos) + cos(3.14552 * relPos) + cos(4.6441 * relPos) + cos(5.9091 * relPos);
	float adjRand = 1. - 0.4 * random(relPos);

	float b = .5 + .25 * (wot.x * adjRand + wot.y * adjRand);
	out_Color = vec4(b, b, b, 1) * modColor;
}
