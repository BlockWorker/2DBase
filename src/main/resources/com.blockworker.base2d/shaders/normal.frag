#version 330 core

uniform sampler2D texture_diffuse;
uniform vec4 modColor;

in vec4 pass_Color;
in vec2 pass_TexCoord;

out vec4 out_Color;

void main(void) {
    vec4 tex_Color = texture(texture_diffuse, pass_TexCoord);
    out_Color.r = tex_Color.r * pass_Color.r * modColor.r;
    out_Color.g = tex_Color.g * pass_Color.g * modColor.g;
    out_Color.b = tex_Color.b * pass_Color.b * modColor.b;
    out_Color.a = tex_Color.a * pass_Color.a * modColor.a;
}
