package com.blockworker.base2d

import com.blockworker.base2d.core.BaseSettings._
import com.blockworker.base2d.core.updating.UpdateManager
import com.blockworker.base2d.core._
import com.blockworker.base2d.file.SettingsFile
import com.blockworker.base2d.gui.{GuiBasicText, GuiTextBox}
import com.blockworker.base2d.input._
import com.blockworker.base2d.render.{ObjModel, RenderUtils}
import com.blockworker.base2d.sprite.RandomShaderSprite
import org.lwjgl.{BufferUtils, Version}
import org.lwjgl.glfw.GLFW._
import org.lwjgl.glfw.{GLFWErrorCallback, GLFWVidMode}
import org.lwjgl.opengl.GL11._
import org.lwjgl.opengl.{GL, GL20}
import org.lwjgl.system.MemoryUtil._

import scala.collection.mutable

/**
  * Created by Alex on 16.06.2016.
  */
object Main {

  // General Vars
  var gameName: String = "2DBase"

  var graphicsThread: Thread = _
  var logicThread: Thread = _

  var settingsPath: String = "settings.tag"

  var shouldTerminate = false

  // Graphics Vars

  /**
    * Whether the y scale should go top-to-bottom instead of bottom-to-top.
    */
  val topToBottom: Boolean = true

  var multiWindow = false
  val windows = mutable.MutableList[Long](0)
  private var vidmode: GLFWVidMode = _

  var letterboxLeft, letterboxRight, letterboxTop, letterboxBottom = 0
  def letterboxedWidth = windowSize().width - letterboxLeft - letterboxRight
  def letterboxedHeight = windowSize().height - letterboxTop - letterboxBottom

  var framesPerSecond = 0
  /**
    * Total time taken by last frame, in seconds. Useful to make animation speeds independent of frame rate.
    */
  var frameDeltaTime = 0d
  /**
    * Pure processing time of the last frame, in milliseconds. Useful for performance analysis.
    */
  var framePureDelta = 0d

  var fpsView: GuiBasicText = _

  // Logic Vars

  var updatesPerSecond = 0
  /**
    * Total time taken by last update, in seconds. Useful to make process speeds independent of update rate.
    */
  var updateDeltaTime = 0d
  /**
    * Pure processing time of the last update, in milliseconds. Useful for performance analysis.
    */
  var updatePureDelta = 0d
  var maxUpdatesPerSecond = 60
  private def minUpdateTime: Long = math.ceil(1e9d / maxUpdatesPerSecond.toDouble).toInt

  var upsView: GuiBasicText = _

  // Events

  val GraphicsInit = new Event[Unit]
  val LogicInit = new Event[Unit]
  val GraphicsCleanup = new Event[Unit]
  val LogicCleanup = new Event[Unit]
  val LetterboxChanged = new Event[Unit]
  val WindowInit = new Event[Unit]

  /**
    * If true, changing fullscreen, monitor and AA settings will not automatically create a new updated window.
    */
  var manualWinCreation = false


  def main(args: Array[String]): Unit = {
    println("LWJGL version " + Version.getVersion + " loaded.")

    graphicsThread = Thread.currentThread()
    graphicsThread.setName(gameName + " Graphics Thread")
    graphicsThread.setPriority(6)
    logicThread = new Thread(Thread.currentThread().getThreadGroup, () => startLogicThread(), gameName + " Logic Thread")
    logicThread.setPriority(5)

    logicThread.start()

    try {
      SettingsFile.loadSettings()

      fullscreen.ValueChanged += onFullscreenChanged
      windowSize.ValueChanged += onWinSizeChanged
      fullscreenRes.ValueChanged += onFullscreenResChanged
      windowTitle.ValueChanged += onWindowTitleChanged
      showFPS.ValueChanged += onShowFPSChanged
      antialiasing.ValueChanged += onAAChanged
      monitor.ValueChanged += onMonitorChanged

      initGraphics()
      graphicsLoop()
      cleanupGraphics()

      windows.foreach(w => glfwDestroyWindow(w))
    } catch {
      case e: Throwable => e.printStackTrace()
    } finally {
      shouldTerminate = true
      glfwTerminate()
      glfwSetErrorCallback(null)
    }
  }

  private def initGraphics(): Unit = {
    GLFWErrorCallback.createPrint(System.err).set()

    if (!glfwInit()) error("Main", "GLFW - Init failed")

    monitor.setValue(glfwGetPrimaryMonitor(), true)

    glfwDefaultWindowHints()
    glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE)
    glfwWindowHint(GLFW_RESIZABLE, if (fullscreen() == 0) GLFW_TRUE else GLFW_FALSE)
    glfwWindowHint(GLFW_DECORATED, if (fullscreen() > 0) GLFW_FALSE else GLFW_TRUE)
    glfwWindowHint(GLFW_SAMPLES, antialiasing())
    glfwWindowHint(GLFW_CONTEXT_RELEASE_BEHAVIOR, GLFW_RELEASE_BEHAVIOR_NONE)

    createWindow()

    fpsView = new GuiBasicText(1, 0, -255, "FPS: 0", Resources.font_Consolas, 13)
    upsView = new GuiBasicText(1, 13, -255, "UPS: 0", Resources.font_Consolas, 13)

    if (!showFPS()) {
      fpsView.hide()
      upsView.hide()
    }

    //new GuiTextBox(100, 100, 0, 500, 300, false, Resources.font_Consolas, 30, "Hello World!")

    new RandomShaderSprite("yay", 100, 100, 0, 500, 400)

    GraphicsInit(this, Unit)
  }

  private def createWindow(): Unit = {
    vidmode = glfwGetVideoMode(monitor())
    val fs = fullscreen()

    if (fs > 0) windowSize.setValue(GraphicalSize.fromVidMode(vidmode), true)

    val size = if (fs == 1) fullscreenRes() else windowSize()

    val oldWinHandle = windows.head
    if (RenderUtils.loadedTextures.nonEmpty) RenderUtils.disposeTextures()

    windows(0) = glfwCreateWindow(size.width, size.height, Localization.process(windowTitle()), if (fs == 1) monitor() else NULL, NULL)
    if (windows.head == NULL) error("Main", "GLFW - Window creation failed")

    WindowInit(this, Unit)

    glfwMakeContextCurrent(windows.head)
    GL.createCapabilities()

    if (oldWinHandle != NULL) glfwDestroyWindow(oldWinHandle)

    windows.foreach { w =>
      glfwSetKeyCallback(w, KeyboardHandler)
      glfwSetCharModsCallback(w, CharHandler)
      glfwSetMouseButtonCallback(w, MouseHandler)
      glfwSetScrollCallback(w, ScrollHandler)
      glfwSetCursorPosCallback(w, MousePosHandler)
    }
    if (fs == 0) glfwSetWindowSizeCallback(windows.head, WindowSizeHandler)

    updateLetterbox()

    glfwSwapInterval(1)

    glfwShowWindow(windows.head)

    glClearColor(1f, 1f, 1f, 1f)
    glEnable(GL_TEXTURE_2D)
    glEnable(GL_DEPTH_TEST)
    glEnable(GL_SCISSOR_TEST)
    glEnable(GL_BLEND)
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
    glDisable(GL_CULL_FACE)

    RenderUtils.bindDefaultTexture()
    RenderUtils.setupMatrices(topToBottom)
    RenderUtils.setupShaders()

    val mxb, myb = BufferUtils.createIntBuffer(1)
    glfwGetMonitorPos(monitor(), mxb, myb)
    val mx = mxb.get()
    val my = myb.get()

    if (fs == 2) {
      glfwSetWindowPos(windows.head, mx, my)
    } else if (fs == 0) {
      glfwSetWindowPos(windows.head, mx + (vidmode.width() - size.width) / 2, my + (vidmode.height() - size.height) / 2)
    }
  }

  def updateLetterbox(): Unit = {
    val coordRatio = coordRes().ratio
    val winRatio = windowSize().ratio
    val widthDelta = windowSize().width.toDouble - windowSize().height.toDouble * coordRatio
    val heightDelta = windowSize().height.toDouble - windowSize().width.toDouble / coordRatio
    if (winRatio > coordRatio && widthDelta >= 1) {
      letterboxLeft = math.ceil(widthDelta / 2d).toInt
      letterboxRight = math.floor(widthDelta / 2d).toInt
      letterboxTop = 0
      letterboxBottom = 0
    } else if (winRatio < coordRatio && heightDelta >= 1) {
      letterboxLeft = 0
      letterboxRight = 0
      letterboxTop = math.ceil(heightDelta / 2d).toInt
      letterboxBottom = math.floor(heightDelta / 2d).toInt
    } else {
      letterboxLeft = 0
      letterboxRight = 0
      letterboxTop = 0
      letterboxBottom = 0
    }
    glScissor(0, 0, windowSize().width, windowSize().height)
    glClearColor(0, 0, 0, 0)
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    glViewport(letterboxLeft, letterboxBottom, letterboxedWidth, letterboxedHeight)
    defaultScissor()
    LetterboxChanged(this, Unit)
  }

  def defaultScissor(): Unit = glScissor(letterboxLeft, letterboxBottom, letterboxedWidth, letterboxedHeight)

  def onFullscreenChanged(sender: AnyRef, fs: Byte): Unit = {
    glfwWindowHint(GLFW_DECORATED, if (fs > 0) GLFW_FALSE else GLFW_TRUE)
    glfwWindowHint(GLFW_RESIZABLE, if (fs == 0) GLFW_TRUE else GLFW_FALSE)
    if (!manualWinCreation) createWindow()
  }

  def onMonitorChanged(sender: AnyRef, monitor: Long): Unit = {
    if (!manualWinCreation) createWindow()
  }

  def onWinSizeChanged(sender: AnyRef, size: GraphicalSize): Unit = {
    if (WindowSizeHandler.handled) {
      WindowSizeHandler.handled = false
      return
    }
    if (fullscreen() == 1) return
    if (fullscreen() == 2 && size != GraphicalSize.fromVidMode(vidmode)) {
      fullscreen.setValue(0)
      return
    }

    glfwSetWindowSize(windows.head, windowSize().width, windowSize().height)

    vidmode = glfwGetVideoMode(monitor())
    glfwSetWindowPos(windows.head, (vidmode.width() - windowSize().width) / 2, (vidmode.height() - windowSize().height) / 2)

    updateLetterbox()
    glClearColor(1f, 1f, 1f, 1f)
  }

  def onFullscreenResChanged(sender: AnyRef, res: GraphicalSize): Unit = {
    if (fullscreen() != 1) return

    glfwSetWindowSize(windows.head, fullscreenRes().width, fullscreenRes().height)
  }

  def onShowFPSChanged(sender: AnyRef, show: Boolean): Unit = {
    if (show) {
      fpsView.show()
      upsView.show()
    } else {
      fpsView.hide()
      upsView.hide()
    }
  }

  def onWindowTitleChanged(sender: AnyRef, title: String): Unit = glfwSetWindowTitle(windows.head, Localization.process(windowTitle()))

  def onAAChanged(sender: AnyRef, aa: Byte): Unit = {
    glfwWindowHint(GLFW_SAMPLES, aa)
    if (!manualWinCreation) createWindow()
  }

  private def graphicsLoop(): Unit = {
    //var lastUpdate = System.nanoTime()
    while (!glfwWindowShouldClose(windows.head) && !shouldTerminate) {
      val time1 = System.nanoTime()

      RenderUtils.resetShaderProgram()

      RenderUtils.pushFrameMatrices()
      RenderUtils.pushModelMatrix()
      RenderUtils.pushMousePos()

      glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

      if (showFPS()/* && time1 - lastUpdate > 100000000*/) {
        fpsView.setText("FPS: " + framesPerSecond)
        upsView.setText("UPS: " + updatesPerSecond)
        /*lastUpdate = time1*/
      }

      InterThread.executeGraphicsSchedule()

      UpdateManager.doFrameUpdates()

      RenderUtils.disableShaderProgram()

      val time2 = System.nanoTime()

      windows.foreach(w => glfwSwapBuffers(w))
      glfwPollEvents()

      val time3 = System.nanoTime()

      framePureDelta = (time2 - time1).toDouble / 1e6d
      frameDeltaTime = (time3 - time1).toDouble / 1e9d
      framesPerSecond = math.round(1d / frameDeltaTime).toInt
    }
  }

  private def cleanupGraphics(): Unit = {
    RenderUtils.disposeTextures()
    ObjModel.unloadAll()
    GraphicsCleanup(this, Unit)
  }

  private def startLogicThread(): Unit = {
    try {
      initLogic()
      logicLoop()
      cleanupLogic()
    } catch {
      case e: Throwable => e.printStackTrace()
    } finally {
      shouldTerminate = true
    }
  }

  private def initLogic(): Unit = {
    Localization.loadLocale(Resources.domain_2DBase)

    LogicInit(this, Unit)
  }

  private def logicLoop(): Unit = {
    while (!shouldTerminate) {
      val time1 = System.nanoTime()

      InterThread.executeLogicSchedule()

      UpdateManager.doLogicUpdates()

      val time2 = System.nanoTime()

      val dif = minUpdateTime - (time2 - time1)
      val difMils = dif / 1000000
      val difNanos = (dif % 1000000).toInt
      if (dif > 0) Thread.sleep(difMils, difNanos)

      val time3 = System.nanoTime()

      updatePureDelta = (time2 - time1).toDouble / 1e6d
      updateDeltaTime = (time3 - time1).toDouble / 1e9d
      updatesPerSecond = math.round(1d / updateDeltaTime).toInt
    }
  }

  private def cleanupLogic(): Unit = {
    SettingsFile.saveSettings()
    LogicCleanup(this, Unit)
  }

  def error(`class`: String, desc: String): Unit = {
    throw new Exception("Error in class '" + `class` + "' on thread '" + Thread.currentThread().getName + "': " + desc + ".")
  }

}
