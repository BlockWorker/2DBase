package com.blockworker.base2d.file

import java.io.{File, IOException}

import com.blockworker.base2d.Main
import com.blockworker.base2d.core.{Event, GraphicalSize}
import com.blockworker.base2d.core.BaseSettings._

/**
  * Utility object for saving/loading settings.
  */
object SettingsFile {

  val ReadSettings = new Event[TagCollection]
  val WriteSettings = new Event[TagCollection]
  val NoMD5Match = new Event[String]

  def loadSettings(): Unit = {
    val coll = new TagCollection
    if (!new File(Main.settingsPath).exists()) return
    try {
      coll.read(Main.settingsPath)
    } catch {
      case _: IOException => NoMD5Match(this, Main.settingsPath)
        return
    }
    val screen = coll.getIntArray("screen")
    coordRes.setValue(GraphicalSize(screen(0), screen(1)))
    windowSize.setValue(GraphicalSize(screen(2), screen(3)))
    fullscreenRes.setValue(GraphicalSize(screen(4), screen(5)))
    fullscreen.setValue(coll.getByte("fullscreen"))
    monitor.setValue(coll.getLong("monitor"))
    showFPS.setValue(coll.getByte("showFPS") > 0)
    masterVolume.setValue(coll.getFloat("masterVolume"))
    language.setValue(coll.getString("language"))
    antialiasing.setValue(coll.getByte("antialiasing"))
    netHeader.setValue(coll.getByte("netHeader"))
    ReadSettings(this, coll)
  }

  def saveSettings(): Unit = {
    val coll = new TagCollection
    coll.setIntArray("screen", Array[Int](coordRes().width, coordRes().height,
      windowSize().width, windowSize().height, fullscreenRes().width, fullscreenRes().height))
    coll.setByte("fullscreen", fullscreen())
    coll.setLong("monitor", monitor())
    coll.setByte("showFPS", if (showFPS()) 1.toByte else 0.toByte)
    coll.setFloat("masterVolume", masterVolume())
    coll.setString("language", language())
    coll.setByte("antialiasing", antialiasing())
    coll.setByte("netHeader", netHeader())
    WriteSettings(this, coll)
    coll.write(Main.settingsPath)
  }

}
