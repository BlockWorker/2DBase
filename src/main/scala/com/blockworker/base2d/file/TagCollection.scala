package com.blockworker.base2d.file

import java.io.{File, FileInputStream, FileOutputStream, IOException}
import java.security.MessageDigest
import java.util

import scala.collection.mutable
import TagUtils._

/**
  * Created by Alex on 25.03.2017.
  */
class TagCollection {

  protected var bytes = mutable.Map[String, ByteTag]()
  protected var ints = mutable.Map[String, IntTag]()
  protected var longs = mutable.Map[String, LongTag]()
  protected var floats = mutable.Map[String, FloatTag]()
  protected var doubles = mutable.Map[String, DoubleTag]()
  protected var strings = mutable.Map[String, StringTag]()
  protected var byteArrays = mutable.Map[String, ByteArrayTag]()
  protected var intArrays = mutable.Map[String, IntArrayTag]()

  def read(path: String): Unit = {
    val file = new File(path)
    if (!file.exists()) return
    val arr = new Array[Byte](file.length().toInt)
    val str = new FileInputStream(file)
    str.read(arr)
    readFromBytes(arr)
    str.close()
  }

  def readFromBytes(arr: Array[Byte]): Unit = {
    if (!util.Arrays.equals(arr.slice(0, 16), MessageDigest.getInstance("MD5").digest(arr.drop(16))))
      throw new IOException("TagCollection from bytes: MD5 doesn't match")
    var i = 16
    while (i < arr.length) {
      val nl = arr(i); i += 1
      val name = new String(arr.slice(i, i + nl), "UTF-8"); i += nl
      val t = arr(i); i += 1
      t match {
        case BYTE => setByte(name, arr(i)); i += 1
        case INT => setInt(name, intFromBytes(arr, i)); i += 4
        case LONG => setLong(name, longFromBytes(arr, i)); i += 8
        case FLOAT => setFloat(name, java.lang.Float.intBitsToFloat(intFromBytes(arr, i))); i += 4
        case DOUBLE => setDouble(name, java.lang.Double.longBitsToDouble(longFromBytes(arr, i))); i += 8
        case STRING => val sl = arr(i); i += 1
          setString(name, new String(arr.slice(i, i + sl), "UTF-8")); i += sl
        case BYTEARRAY => val al = intFromBytes(arr, i); i += 4
          setByteArray(name, arr.slice(i, i + al)); i += al
        case INTARRAY => val al = intFromBytes(arr, i); i += 4
          setIntArray(name, intArrayFromBytes(arr, i, al)); i += al * 4
      }
    }
  }

  def write(path: String): Unit = {
    val file = new File(path)
    if (file.exists()) file.delete()
    file.createNewFile()
    val str = new FileOutputStream(file)
    str.write(writeToBytes())
    str.close()
  }

  def writeToBytes(): Array[Byte] = {
    var arr = Array[Byte]()
    for ((_, tag) <- bytes ++ ints ++ longs ++ floats ++ doubles ++ strings ++ byteArrays ++ intArrays) {
      arr ++= tag.toByteArray
    }
    MessageDigest.getInstance("MD5").digest(arr) ++ arr
  }

  def setByte(name: String, tag: ByteTag): Unit = bytes(name) = tag
  def setByte(name: String, value: Byte): Unit = setByte(name, new ByteTag(name, value))
  def setInt(name: String, tag: IntTag): Unit = ints(name) = tag
  def setInt(name: String, value: Int): Unit = setInt(name, new IntTag(name, value))
  def setLong(name: String, tag: LongTag): Unit = longs(name) = tag
  def setLong(name: String, value: Long): Unit = setLong(name, new LongTag(name, value))
  def setFloat(name: String, tag: FloatTag): Unit = floats(name) = tag
  def setFloat(name: String, value: Float): Unit = setFloat(name, new FloatTag(name, value))
  def setDouble(name: String, tag: DoubleTag): Unit = doubles(name) = tag
  def setDouble(name: String, value: Double): Unit = setDouble(name, new DoubleTag(name, value))
  def setString(name: String, tag: StringTag): Unit = strings(name) = tag
  def setString(name: String, value: String): Unit = setString(name, new StringTag(name, value))
  def setByteArray(name: String, tag: ByteArrayTag): Unit = byteArrays(name) = tag
  def setByteArray(name: String, value: Array[Byte]): Unit = setByteArray(name, new ByteArrayTag(name, value))
  def setIntArray(name: String, tag: IntArrayTag): Unit = intArrays(name) = tag
  def setIntArray(name: String, value: Array[Int]): Unit = setIntArray(name, new IntArrayTag(name, value))

  def getByteTag(name: String): ByteTag = bytes(name)
  def getByte(name: String): Byte = getByteTag(name).value
  def getIntTag(name: String): IntTag = ints(name)
  def getInt(name: String): Int = getIntTag(name).value
  def getLongTag(name: String): LongTag = longs(name)
  def getLong(name: String): Long = getLongTag(name).value
  def getFloatTag(name: String): FloatTag = floats(name)
  def getFloat(name: String): Float = getFloatTag(name).value
  def getDoubleTag(name: String): DoubleTag = doubles(name)
  def getDouble(name: String): Double = getDoubleTag(name).value
  def getStringTag(name: String): StringTag = strings(name)
  def getString(name: String): String = getStringTag(name).value
  def getByteArrayTag(name: String): ByteArrayTag = byteArrays(name)
  def getByteArray(name: String): Array[Byte] = getByteArrayTag(name).value
  def getIntArrayTag(name: String): IntArrayTag = intArrays(name)
  def getIntArray(name: String): Array[Int] = getIntArrayTag(name).value

}
