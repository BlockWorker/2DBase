package com.blockworker.base2d.file

import java.io.{IOException, InputStream}

import TagUtils._

/**
  * Created by Alex on 25.03.2017.
  */
object TagUtils {

  val BYTE = 0x01.toByte
  val INT = 0x02.toByte
  val LONG = 0x03.toByte
  val FLOAT = 0x04.toByte
  val DOUBLE = 0x05.toByte
  val STRING = 0x06.toByte
  val BYTEARRAY = 0x07.toByte
  val INTARRAY = 0x08.toByte

  def stringBytes(string: String): Array[Byte] = {
    val b = string.getBytes("UTF-8")
    b.length.toByte +: b
  }
  def intBytes(int: Int): Array[Byte] = Array[Byte](((int & 0xFF000000) >>> 24).toByte, ((int & 0xFF0000) >>> 16).toByte, ((int & 0xFF00) >>> 8).toByte, (int & 0xFF).toByte)
  def longBytes(long: Long): Array[Byte] = intBytes(((long & 0xFFFFFFFF00000000L) >> 32).toInt) ++ intBytes((long & 0xFFFFFFFF).toInt)
  def intArrayBytes(arr: Array[Int]): Array[Byte] = {
    var ret = intBytes(arr.length)
    for (i <- arr) ret ++= intBytes(i)
    ret
  }

  def intFromBytes(bytes: Array[Byte], index: Int): Int = (bytes(index) << 24) + ((bytes(index + 1) << 16) & 0xFF0000) + ((bytes(index + 2) << 8) & 0xFF00) + (bytes(index + 3).toInt & 0xFF)
  def longFromBytes(bytes: Array[Byte], index: Int): Long = (intFromBytes(bytes, index).toLong << 32L) + intFromBytes(bytes, index + 4)
  def intArrayFromBytes(bytes: Array[Byte], index: Int, length: Int): Array[Int] = {
    var ret = Array[Int]()
    for (i <- 0 until length) ret :+= intFromBytes(bytes, index + 4 * i)
    ret
  }

}

trait Tag { def toByteArray: Array[Byte] }

class ByteTag(val name: String, val value: Byte) extends Tag {

  override def toByteArray: Array[Byte] = stringBytes(name) ++ Array[Byte](BYTE, value)

}

class IntTag(val name: String, val value: Int) extends Tag {

  override def toByteArray: Array[Byte] = (stringBytes(name) :+ INT) ++ intBytes(value)

}

class LongTag(val name: String, val value: Long) extends Tag {

  override def toByteArray: Array[Byte] = (stringBytes(name) :+ LONG) ++ longBytes(value)

}

class FloatTag(val name: String, val value: Float) extends Tag {

  override def toByteArray: Array[Byte] = (stringBytes(name) :+ FLOAT) ++ intBytes(java.lang.Float.floatToIntBits(value))

}

class DoubleTag(val name: String, val value: Double) extends Tag {

  override def toByteArray: Array[Byte] = (stringBytes(name) :+ DOUBLE) ++ longBytes(java.lang.Double.doubleToLongBits(value))

}

class StringTag(val name: String, val value: String) extends Tag {

  override def toByteArray: Array[Byte] = (stringBytes(name) :+ STRING) ++ stringBytes(value)

}

class ByteArrayTag(val name: String, val value: Array[Byte]) extends Tag {

  override def toByteArray: Array[Byte] = (stringBytes(name) :+ BYTEARRAY) ++ intBytes(value.length) ++ value

  def asFile = new IncludedFileStream(value)

}

class IntArrayTag(val name: String, val value: Array[Int]) extends Tag {

  override def toByteArray: Array[Byte] = (stringBytes(name) :+ INTARRAY) ++ intArrayBytes(value)

}

class IncludedFileStream(val bytes: Array[Byte]) extends InputStream {
  private var pos = 0
  private var mark = 0
  private var readlimit = -1

  override def read(): Int = {
    if (readlimit >= 0) readlimit -= 1
    if (pos >= bytes.length) -1
    else bytes(pos)
  }

  override def available(): Int = bytes.length - pos

  override def markSupported(): Boolean = true

  override def mark(readlimit: Int): Unit = {
    mark = pos
    this.readlimit = readlimit
  }

  override def reset(): Unit = {
    if (readlimit < 0) throw new IOException("Stream not marked or mark expired")
    pos = mark
  }
}
