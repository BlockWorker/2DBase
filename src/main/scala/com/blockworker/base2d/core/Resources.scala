package com.blockworker.base2d.core

/**
  * Created by Alex on 21.01.2017.
  */
object Resources {

  def getResourcePath(domain: String, subpath: String, file: String): String = "res/" + domain + "/" + subpath + "/" + file

  def getTexturePath(domain: String, file: String): String = getResourcePath(domain, "textures", file)
  @Deprecated def getFontTexturePath(domain: String, file: String): String = getResourcePath(domain, "textures/font", file)
  def getShaderPath(domain: String, file: String): String = getResourcePath(domain, "shaders", file)
  def getSoundPath(domain: String, file: String): String = getResourcePath(domain, "sounds", file)
  def getLocalePath(domain: String, file: String): String = getResourcePath(domain, "locale", file)
  def getModelPath(domain: String, file: String): String = getResourcePath(domain, "models", file)
  def getFontPath(domain: String, file: String): String = getResourcePath(domain, "models/font", file)

  // Useful paths
  val domain_2DBase = "com.blockworker.base2d"
  val shader_normalVert = getShaderPath(domain_2DBase, "normal.vert")
  val shader_normalFrag = getShaderPath(domain_2DBase, "normal.frag")
  val font_Consolas = getFontPath(domain_2DBase, "Consolas.obj")
}
