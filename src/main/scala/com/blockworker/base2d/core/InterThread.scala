package com.blockworker.base2d.core

import com.blockworker.base2d.Main

import scala.collection.mutable

/**
  * Tool for delegating tasks to the other thread and establishing thread safety.
  * Whatever gets scheduled for a thread will be executed at the start of that thread's next update.
  */
object InterThread {

  protected val graphicsSchedule = mutable.Set[Runnable]()
  protected val logicSchedule = mutable.Set[Runnable]()

  def scheduleGraphics(packet: Runnable): Unit = {
    graphicsSchedule.add(packet)
  }

  def scheduleLogic(packet: Runnable): Unit = {
    logicSchedule.add(packet)
  }

  def executeGraphicsSchedule(): Unit = {
    graphicsSchedule.foreach(e => e.run())
    graphicsSchedule.clear()
  }

  def executeLogicSchedule(): Unit = {
    logicSchedule.foreach(e => e.run())
    logicSchedule.clear()
  }

  def isGraphics = Thread.currentThread() == Main.graphicsThread
  def isLogic = Thread.currentThread() == Main.logicThread

  sealed trait Thread {
    def isCurrent: Boolean
    def getThread: java.lang.Thread
    def delegateTo(packet: Runnable): Unit
  }
  case object Graphics extends Thread {
    override def isCurrent: Boolean = isGraphics
    override def getThread: java.lang.Thread = Main.graphicsThread
    override def delegateTo(packet: Runnable): Unit = if (isCurrent) packet.run() else scheduleGraphics(packet)
  }
  case object Logic extends Thread {
    override def isCurrent: Boolean = isLogic
    override def getThread: java.lang.Thread = Main.logicThread
    override def delegateTo(packet: Runnable): Unit = if (isCurrent) packet.run() else scheduleLogic(packet)
  }
  case object Any extends Thread {
    override def isCurrent: Boolean = true
    override def getThread: java.lang.Thread = null
    override def delegateTo(packet: Runnable): Unit = if (isLogic || isGraphics) packet.run() else scheduleLogic(packet)
  }

}
