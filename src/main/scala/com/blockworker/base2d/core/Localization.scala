package com.blockworker.base2d.core

import java.io._

import com.blockworker.base2d.Main

import scala.collection.mutable

/**
  * Object which provides localization functions.
  */
object Localization {
  protected val localeMap: mutable.Map[String, String] = mutable.Map[String, String]()

  val LocaleChanged = new Event[Unit]

  def loadLocale(domain: String): Unit = {
    val localeFile = new File(Resources.getLocalePath(domain, BaseSettings.language() + ".txt"))
    if (!localeFile.exists() || !localeFile.isFile || localeFile.length() == 0) Main.error("Localization", "Localization file empty")
    val reader = new BufferedReader(new FileReader(localeFile))
    var lines = mutable.IndexedSeq[String]()
    try {
      var s = false
      while (!s) {
        val ln = reader.readLine()
        if (ln == null) s = true
        else lines :+= ln
      }
    } finally {
      reader.close()
    }
    lines.foreach { s =>
      val pos = s.indexOf("=")
      if (!s.startsWith("#") && pos > 0) {
        val s1 = s.substring(0, pos)
        val s2 = s.substring(pos + 1)
        localeMap(s1) = s2
      }
    }
    LocaleChanged(this, Unit)
  }

  /**
    * @param name Name of a localization entry
    * @return Requested localization entry, if it exists
    */
  def L(name: String): String = {
    if (localeMap.isDefinedAt(name)) localeMap(name)
    else "<!! " + name + ">"
  }

  /**
    * Replaces every "§L/locale_entry" with the localization entry "locale_entry"
    */
  def process(raw: String): String = {
    val reg = """§L/(\w+)""".r
    reg.replaceAllIn(raw, m => L(m.group(1)))
  }

}
