package com.blockworker.base2d.core.updating

/**
  * Defines an object that updates every updateInterval logic updates.
  */
trait ILogicUpdating {

  var updateInterval: Int

  /**
    * Whether this object should update.
    */
  var updateEnabled: Boolean

  /**
    * Called every updateInterval updates if this is registered in [[com.blockworker.base2d.core.updating.UpdateManager UpdateManager]] and updateEnabled is true.
    */
  def update(): Unit

}
