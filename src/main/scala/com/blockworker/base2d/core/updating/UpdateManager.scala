package com.blockworker.base2d.core.updating

import scala.collection.mutable

/**
  * Manages all objects that request updates on render and/or logic ticks.
  */
object UpdateManager {

  var frameUpdates = mutable.Seq[IFrameUpdating]()
  var logicUpdates = mutable.Map[Int, mutable.Buffer[ILogicUpdating]]()

  var maxLogicIntv = 1
  var logicCounter = 0

  def registerFrameUpdate(update: IFrameUpdating): Unit = frameUpdates :+= update

  def registerLogicUpdate(update: ILogicUpdating): Unit = {
    val updateIntv = update.updateInterval
    if (logicUpdates.contains(updateIntv)) {
      if (logicUpdates(updateIntv).contains(update)) {
        return
      } else {
        logicUpdates(updateIntv) += update
      }
    } else {
      logicUpdates(updateIntv) = mutable.Buffer(update)
    }
    maxLogicIntv = logicUpdates.keySet.max
  }

  def unregisterFrameUpdate(update: IFrameUpdating): Unit = frameUpdates = frameUpdates.filterNot(f => f == update)

  def unregisterLogicUpdate(update: ILogicUpdating): Unit = {
    val updateIntv = update.updateInterval
    if (logicUpdates.contains(updateIntv) && logicUpdates(updateIntv).contains(update)) {
      logicUpdates(updateIntv) -= update
      if (logicUpdates(updateIntv).isEmpty) logicUpdates.remove(updateIntv)
    }
    maxLogicIntv = logicUpdates.keySet.max
  }

  def doFrameUpdates(): Unit = {
    frameUpdates.sortWith(_.getZ > _.getZ).foreach(f => if (f.renderEnabled) f.render())
  }

  def doLogicUpdates(): Unit = {
    logicCounter += 1
    logicUpdates.foreach { iv =>
      if (logicCounter % iv._1 == 0) {
        iv._2.foreach { lu =>
          if (lu.updateEnabled) lu.update()
        }
      }
    }
    if (logicCounter == maxLogicIntv) logicCounter = 0
  }

}
