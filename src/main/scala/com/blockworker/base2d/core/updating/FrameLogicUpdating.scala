package com.blockworker.base2d.core.updating

/**
  * Created by Alex on 16.08.2016.
  */
abstract class FrameLogicUpdating(override var updateInterval: Int = 1) extends IFrameUpdating with ILogicUpdating{

  UpdateManager.registerFrameUpdate(this)
  UpdateManager.registerLogicUpdate(this)

  override var renderEnabled: Boolean = true
  override var updateEnabled: Boolean = true

  /**
    * Called every frame if this is registered in UpdateManager and renderEnabled is true.
    */
  override def render(): Unit

  /**
    * Called every [[updateInterval]] updates if this is registered in [[UpdateManager]] and [[updateEnabled]] is true.
    */
  override def update(): Unit
}
