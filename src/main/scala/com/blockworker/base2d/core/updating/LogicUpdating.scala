package com.blockworker.base2d.core.updating

/**
  * Defines an object that updates every updateInterval logic updates.
  */
abstract class LogicUpdating(override var updateInterval: Int = 1) extends ILogicUpdating {

  UpdateManager.registerLogicUpdate(this)

  /**
    * Whether this object should update.
    */
  override var updateEnabled: Boolean = true

  /**
    * Called every updateInterval updates if this is registered in [[com.blockworker.base2d.core.updating.UpdateManager UpdateManager]] and updateEnabled is true.
    */
  override def update(): Unit
}
