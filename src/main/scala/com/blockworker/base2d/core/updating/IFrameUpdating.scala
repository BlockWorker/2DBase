package com.blockworker.base2d.core.updating

import com.blockworker.base2d.sprite.ScreenObject
import org.lwjgl.opengl.GL11._

/**
  * Created by Alex on 14.08.2016.
  */
trait IFrameUpdating extends ScreenObject {

  /**
    * Whether this object should render.
    */
  var renderEnabled: Boolean

  /**
    * Called every frame if this is registered in [[UpdateManager]] and [[renderEnabled]] is true.
    */
  def render(): Unit

  override def show(): Unit = renderEnabled = true

  override def hide(): Unit = renderEnabled = false

  override def isVisible: Boolean = renderEnabled

}
