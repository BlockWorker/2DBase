package com.blockworker.base2d.core.updating

/**
  * Created by Alex on 16.08.2016.
  */
abstract class FrameUpdating extends IFrameUpdating {

  UpdateManager.registerFrameUpdate(this)

  /**
    * Whether this object should render.
    */
  override var renderEnabled: Boolean = true

  /**
    * Called every frame if this is registered in [[com.blockworker.base2d.core.updating.UpdateManager UpdateManager]] and renderEnabled is true.
    */
  override def render(): Unit
}
