package com.blockworker.base2d.core

/**
  * Game settings controller.
  */
object BaseSettings {
  val coordRes = new Setting[GraphicalSize](GraphicalSize(1920, 1080), InterThread.Graphics)
  val windowSize = new Setting[GraphicalSize](GraphicalSize(1280, 720), InterThread.Graphics)
  val fullscreenRes = new Setting[GraphicalSize](GraphicalSize(1920, 1080), InterThread.Graphics)
  val fullscreen = new Setting[Byte](0, InterThread.Graphics)
  val windowTitle = new Setting[String]("2DBase", InterThread.Graphics)
  val monitor = new Setting[Long](0, InterThread.Graphics)
  val showFPS = new Setting[Boolean](true, InterThread.Graphics)
  val masterVolume = new Setting[Float](-6f, InterThread.Logic)
  val language = new Setting[String]("en_US", InterThread.Logic)
  val antialiasing = new Setting[Byte](16, InterThread.Graphics)
  val netHeader = new Setting[Byte](0x01, InterThread.Logic)
}

class Setting[T](val defaultValue: T, val setScope: InterThread.Thread) {
  protected var value: T = defaultValue

  val ValueChanged = new Event[T]

  /**
    * @param silent If true, will not call the ValueChanged event. WARNING: Only use this if you know what you're doing!
    */
  def setValue(newValue: T, silent: Boolean = false): Unit = {
    setScope.delegateTo(() => { value = newValue; if (!silent) ValueChanged(this, value) })
  }

  def getValue = value

  def apply() = value
}
