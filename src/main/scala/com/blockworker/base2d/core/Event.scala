package com.blockworker.base2d.core

/**
  * Defines an event which can be fired and listened for.
  * @tparam EventArgs Type of the event arguments that are provided to the listeners.
  */
class Event[EventArgs] {

  protected var listeners = List[(AnyRef, EventArgs) => Any]()

  /**
    * Fires the event.
    */
  def apply(sender: AnyRef, e: EventArgs): Unit = listeners.foreach(l => l(sender, e))

  /**
    * Adds a listener to the event.
    */
  def +=(elem: (AnyRef, EventArgs) => Any): Unit = listeners :+= elem

  /**
    * Removes a listener from the event.
    */
  def -=(elem: (AnyRef, EventArgs) => Any): Unit = listeners = listeners.filter(l => l != elem)

}
