package com.blockworker.base2d.core

import org.lwjgl.glfw.GLFWVidMode

/**
  * Created by Alex on 17.03.2017.
  */
object GraphicalSize {

  def fromVidMode(vidmode: GLFWVidMode): GraphicalSize = {
    var gs: GraphicalSize = null
    InterThread.Graphics.delegateTo(() => {
      gs = GraphicalSize(vidmode.width(), vidmode.height())
    })
    gs
  }

}

case class GraphicalSize(width: Int, height: Int) {

  def withWidth(newWidth: Int) = GraphicalSize(newWidth, height)
  def withHeight(newHeight: Int) = GraphicalSize(width, newHeight)
  def ratio = width.toDouble / height.toDouble

}
