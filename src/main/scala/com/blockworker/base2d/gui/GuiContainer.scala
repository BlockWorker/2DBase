package com.blockworker.base2d.gui

import com.blockworker.base2d.Main
import com.blockworker.base2d.core.updating.UpdateManager
import com.blockworker.base2d.render.ScreenSpaceRect

import scala.collection.mutable

/**
  * Defines a GUI item container.
  */
class GuiContainer(protected var xPos: Float, protected var yPos: Float, protected var zPos: Float,
                   protected var width: Float, protected var height: Float, protected var window: Long = Main.windows.head) extends GuiItem {

  protected var visible = true
  protected var scissorBounds = ScreenSpaceRect.rectFromCoords(xPos, yPos, width, height)
  protected val content = new Content(xPos, yPos, zPos, window, scissorBounds)

  Main.LetterboxChanged += createScissor

  override def hide(): Unit = {
    visible = false
    content.hide()
  }

  override def show(): Unit = {
    visible = true
    content.show()
  }

  override def dispose(): Unit = {
    Main.LetterboxChanged -= createScissor
    content.dispose()
    UpdateManager.unregisterLogicUpdate(this)
  }

  def addItem(item: GuiItem): Unit = content.addItem(item)

  def removeItem(item: GuiItem): Unit = content.removeItem(item)

  override def setPos(x: Float, y: Float, z: Float): Unit = {
    content.setPosRelative(x - xPos, y - yPos, z - zPos)
    xPos = x
    yPos = y
    zPos = z
    createScissor()
  }

  override def getX: Float = xPos
  override def getY: Float = yPos
  override def getZ: Float = zPos
  override def getWidth: Float = width
  override def getHeight: Float = height

  override def isVisible: Boolean = visible

  def createScissor(sender: AnyRef = null, unit: Unit = Unit): Unit = {
    setScissor(ScreenSpaceRect.rectFromCoords(xPos, yPos, width, height))
  }

  override def setScissor(bounds: ScreenSpaceRect): Unit = {
    scissorBounds = bounds
    content.setScissor(scissorBounds)
  }

  override def getScissor: ScreenSpaceRect = scissorBounds

  override def setWindow(window: Long): Unit = {
    this.window = window
    content.setWindow(window)
  }

  override def getWindow: Long = window


  class Content(protected var xPos: Float, protected var yPos: Float, protected var zPos: Float,
                protected var window: Long, protected var scissorBounds: ScreenSpaceRect) extends GuiItem {

    protected val items = mutable.Set[GuiItem]()
    protected var visible = true

    override def hide(): Unit = {
      visible = false
      items.foreach(e => e.hide())
    }

    override def show(): Unit = {
      visible = true
      items.foreach(e => e.show())
    }

    override def dispose(): Unit = {
      items.foreach(i => i.dispose())
      UpdateManager.unregisterLogicUpdate(this)
    }

    def addItem(item: GuiItem): Unit = {
      item.setScissor(scissorBounds)
      items.add(item)
    }

    def removeItem(item: GuiItem): Unit = {
      items.remove(item)
    }

    override def setScissor(bounds: ScreenSpaceRect): Unit = {
      scissorBounds = bounds
      items.foreach(i => i.setScissor(scissorBounds))
    }
    override def getScissor: ScreenSpaceRect = scissorBounds

    override def setWindow(window: Long): Unit = {
      this.window = window
      items.foreach(i => i.setWindow(window))
    }
    override def getWindow: Long = window

    override def setPos(x: Float, y: Float, z: Float): Unit = {
      items.foreach(i => i.setPosRelative(x - xPos, y - yPos, z - zPos))
      xPos = x
      yPos = y
      zPos = z
    }

    override def getX: Float = xPos
    override def getY: Float = yPos
    override def getZ: Float = zPos
    override def getWidth: Float = 0
    override def getHeight: Float = 0
    override def isVisible: Boolean = visible

    def getItems = items.toSet

  }

}
