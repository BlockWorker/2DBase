package com.blockworker.base2d.gui

import com.blockworker.base2d.core.Event
import org.lwjgl.glfw.GLFW

/**
  * Simple button implementation with no sprites by default.
  */
abstract class GuiButton(x: Float, y: Float, z: Float, wid: Float, hei: Float) extends InteractiveGuiBase(x, y, z, wid, hei) {

  val MouseDown = new Event[Int]
  val MouseUp = new Event[Int]

  override def onMouseButton(x: Float, y: Float, button: Int, action: Int): Unit = {
    if (!visible || !enabled || !inClickArea(x, y)) return
    if (action == GLFW.GLFW_PRESS) onMouseDown(button)
    else onMouseUp(button)
  }

  protected def inClickArea(x: Float, y: Float): Boolean

  protected def onMouseDown(button: Int): Unit = {
    MouseDown(this, button)
  }

  protected def onMouseUp(button: Int): Unit = {
    MouseUp(this, button)
  }

}
