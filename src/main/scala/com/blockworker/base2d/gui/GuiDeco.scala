package com.blockworker.base2d.gui
import com.blockworker.base2d.Main
import com.blockworker.base2d.core.updating.UpdateManager
import com.blockworker.base2d.render.ScreenSpaceRect
import com.blockworker.base2d.sprite.Sprite
import org.joml.{Vector3f, Vector4f}

/**
  * Decorative GuiItem. Should not have any interactive functionality.
  */
class GuiDeco(protected var xPos: Float, protected var yPos: Float, protected var zPos: Float,
              protected var width: Float, protected var height: Float, val items: Sprite*) extends GuiItem {

  protected var scissorBounds: ScreenSpaceRect = _
  protected var window: Long = Main.windows.head
  protected var visible = true
  val offsets = Map(items.map(i => (i.name, new Vector3f(i.getX, i.getY, i.getZ))): _*)
  val alphas = Map(items.map(i => (i.name, if (i.getColor == null) 1f else i.getColor.w)): _*)
  var globalAlpha = 1f
  setPos(xPos, yPos, zPos)

  override def hide(): Unit = {
    visible = false
    items.foreach(i => i.hide())
  }

  override def show(): Unit = {
    visible = true
    items.foreach(i => i.show())
  }

  override def setPos(x: Float, y: Float, z: Float): Unit = {
    xPos = x
    yPos = y
    zPos = z
    items.foreach(i => i.setPos(new Vector3f(offsets(i.name)).add(xPos, yPos, zPos)))
  }

  override def setColor(color: Vector4f): Unit = {
    globalAlpha = color.w
    items.foreach { i =>
      if (i.getColor == null) i.setColor(new Vector4f(1, 1, 1, globalAlpha))
      else {
        val clr = new Vector4f(i.getColor)
        clr.w = alphas(i.name) * globalAlpha
        i.setColor(clr)
      }
    }
  }

  override def dispose(): Unit = {
    items.foreach(i => i.dispose())
    UpdateManager.unregisterLogicUpdate(this)
  }

  override def setScissor(bounds: ScreenSpaceRect): Unit = {
    scissorBounds = bounds
    items.foreach(i => i.setScissorBounds(scissorBounds))
  }
  override def getScissor: ScreenSpaceRect = scissorBounds

  override def setWindow(window: Long): Unit = {
    this.window = window
    items.foreach(i => i.setWindow(window))
  }

  override def getWindow: Long = window

  override def getX: Float = xPos
  override def getY: Float = yPos
  override def getZ: Float = zPos
  override def getWidth: Float = width
  override def getHeight: Float = height
  override def isVisible: Boolean = visible
  override def getColor: Vector4f = new Vector4f(1, 1, 1, globalAlpha)
}
