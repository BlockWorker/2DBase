package com.blockworker.base2d.gui

import com.blockworker.base2d.core.updating.LogicUpdating
import com.blockworker.base2d.render.ScreenSpaceRect
import com.blockworker.base2d.sprite.ScreenObject

/**
  * Defines a GUI item.
  */
trait GuiItem extends LogicUpdating with ScreenObject {

  updateEnabled = false

  override def update(): Unit = {}

  def dispose(): Unit

  def setScissor(bounds: ScreenSpaceRect): Unit

  def getScissor: ScreenSpaceRect

  def setWindow(window: Long): Unit

  def getWindow: Long

}
