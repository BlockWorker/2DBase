package com.blockworker.base2d.gui

import com.blockworker.base2d.core.Event
import org.lwjgl.glfw.GLFW

/**
  * Simple CheckBox implementation with no sprites by default.
  */
abstract class GuiCheckBox(x: Float, y: Float, z: Float, wid: Float, hei: Float) extends InteractiveGuiBase(x, y, z, wid, hei) {

  protected var checked = false

  val CheckChanged = new Event[Boolean]

  override def onMouseButton(x: Float, y: Float, button: Int, action: Int): Unit = {
    if ((button != GLFW.GLFW_MOUSE_BUTTON_LEFT && button != GLFW.GLFW_MOUSE_BUTTON_RIGHT) || action != GLFW.GLFW_PRESS
        || !inClickArea(x, y)) return
    checked = !checked
    onCheckChanged()
  }

  protected def inClickArea(x: Float, y: Float): Boolean

  def setChecked(check: Boolean): Unit = {
    checked = check
    onCheckChanged()
  }
  def isChecked = checked

  protected def onCheckChanged(): Unit = {
    CheckChanged(this, checked)
  }

}
