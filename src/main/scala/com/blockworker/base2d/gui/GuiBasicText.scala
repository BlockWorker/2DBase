package com.blockworker.base2d.gui

import com.blockworker.base2d.Main
import com.blockworker.base2d.core.Localization._
import com.blockworker.base2d.render.{Color, ScreenSpaceRect}
import com.blockworker.base2d.sprite.TextSprite
import org.joml.Vector4f

/**
  * A gui item which consists of only text. Intended for use as a label.
  */
class GuiBasicText(protected var xPos: Float, protected var yPos: Float, protected var zPos: Float, protected var text: String,
                   protected var fontPath: String, protected var scale: Float, protected var color: Vector4f = Color.black,
                   protected var window: Long = Main.windows.head, protected var scissorBounds: ScreenSpaceRect = null) extends GuiItem {

  LocaleChanged += onLocaleUpdate

  protected val sprite = new TextSprite("GuiText Sprite", text, scale, color,
                                        fontPath, xPos, yPos, zPos)
  sprite.setWindow(window)
  sprite.setScissorBounds(scissorBounds)
  sprite.setText(process(text))

  override def hide(): Unit = sprite.hide()

  override def show(): Unit = sprite.show()

  override def setPos(x: Float, y: Float, z: Float): Unit = {
    xPos = x
    yPos = y
    zPos = z
    sprite.setPos(xPos, yPos, zPos)
  }

  def setText(txt: String): Unit = {
    text = txt
    sprite.setText(process(text))
  }
  def getText = text

  def setFont(font: String): Unit = {
    fontPath = font
    sprite.setFontPath(fontPath)
  }
  def getFont = fontPath

  def setScale(scl: Float): Unit = {
    scale = scl
    sprite.setScale(scale)
  }
  def getScale = scale

  def onLocaleUpdate(sender: AnyRef, unit: Unit): Unit = {
    sprite.setText(process(text))
  }

  override def setColor(color: Vector4f): Unit = {
    this.color = color
    sprite.setColor(color)
  }
  override def getColor: Vector4f = new Vector4f(color)

  override def setScissor(bounds: ScreenSpaceRect): Unit = {
    scissorBounds = bounds
    sprite.setScissorBounds(scissorBounds)
  }
  override def getScissor = scissorBounds

  override def setWindow(window: Long): Unit = this.window = window

  override def getWindow: Long = window

  override def dispose(): Unit = {
    LocaleChanged -= onLocaleUpdate
    sprite.dispose()
  }

  override def getX = xPos
  override def getY = yPos
  override def getZ = zPos
  override def getWidth = sprite.getWidth
  override def getHeight = sprite.getHeight
  override def getDepth: Float = sprite.getDepth
  override def isVisible: Boolean = sprite.isVisible
}
