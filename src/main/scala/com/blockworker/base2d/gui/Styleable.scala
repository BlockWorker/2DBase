package com.blockworker.base2d.gui

import com.blockworker.base2d.core.Setting

/**
  * Created by Alex on 19.03.2017.
  * Allows an object to have parameters like colors, fonts etc. that can be changed dynamically using a [[com.blockworker.base2d.core.Setting Setting]].
  * @tparam T Type of the setting which describes this object's style.
  */
trait Styleable[T] {

  def initStyleable(): Unit = {
    if (getStyleSetting == null) return
    getStyleSetting.ValueChanged += onStyleChanged
  }

  def disposeStyleable(): Unit = {
    if (getStyleSetting == null) return
    getStyleSetting.ValueChanged -= onStyleChanged
  }

  def onStyleChanged(sender: AnyRef, style: T)

  def setStyleSetting(setting: Setting[T]): Unit
  def getStyleSetting: Setting[T]

}
