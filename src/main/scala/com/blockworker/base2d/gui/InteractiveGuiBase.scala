package com.blockworker.base2d.gui

import com.blockworker.base2d.Main
import com.blockworker.base2d.core.updating.UpdateManager
import com.blockworker.base2d.input.{MouseHandler, MousePosHandler, MouseReactions}
import com.blockworker.base2d.render.ScreenSpaceRect

/**
  * Created by Alex on 02.01.2017.
  */
abstract class InteractiveGuiBase(protected var xPos: Float, protected var yPos: Float, protected var zPos: Float,
                         protected var width: Float, protected var height: Float, mouseMove: Boolean = false) extends GuiItem with MouseReactions {

  protected var visible = true
  protected var enabled = true
  protected var scissorBounds: ScreenSpaceRect = _
  protected var window: Long = Main.windows.head

  clickRegister()
  hoverRegister(mouseMove)

  override def hide(): Unit = {
    visible = false
    onHide()
  }

  protected def onHide(): Unit

  override def show(): Unit = {
    visible = true
    onShow()
  }

  protected def onShow(): Unit

  def setEnabled(enable: Boolean): Unit = {
    enabled = enable
    if (!enable) onHoverLeave()
  }
  def isEnabled = enabled

  override def setScissor(bounds: ScreenSpaceRect): Unit = {
    scissorBounds = bounds
    onScissorSet()
  }
  override def getScissor: ScreenSpaceRect = scissorBounds

  protected def onScissorSet(): Unit

  override def setWindow(window: Long): Unit = {
    this.window = window
    onWindowSet()
  }
  override def getWindow: Long = window

  protected def onWindowSet(): Unit

  override def dispose(): Unit = {
    MouseHandler.unregisterClickable(this)
    MousePosHandler.unregisterHoverable(this)
    onDispose()
    UpdateManager.unregisterLogicUpdate(this)
  }

  protected def onDispose(): Unit

  override def setPos(x: Float, y: Float, z: Float): Unit = {
    xPos = x
    yPos = y
    zPos = z
    onPosChanged()
  }

  protected def onPosChanged(): Unit

  override def getX: Float = xPos
  override def getY: Float = yPos
  override def getZ: Float = zPos
  override def getWidth: Float = width
  override def getHeight: Float = height
  override def isVisible: Boolean = visible

}
