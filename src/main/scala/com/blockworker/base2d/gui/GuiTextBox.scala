package com.blockworker.base2d.gui

import java.awt.Toolkit
import java.awt.datatransfer.{DataFlavor, StringSelection}

import com.blockworker.base2d.Main
import com.blockworker.base2d.input._
import com.blockworker.base2d.render.Color
import Color.ColorHelpers
import com.blockworker.base2d.sprite.{SingleColorSprite, TextSprite}
import org.joml.Vector4f
import org.lwjgl.glfw.GLFW._
import com.blockworker.base2d.core.{InterThread, Localization}

import scala.collection.mutable

/**
  * Created by Alex on 03.11.2016.
  * @param defaultText Default text, localization placeholders will be resolved.
  */
class GuiTextBox(x: Float, y: Float, z: Float, maxWidth: Float, maxHeight: Float, protected var singleLine: Boolean,
                 protected var font: String, protected var scale: Float, protected var defaultText: String = "",
                 protected var textColor: Vector4f = Color.black, protected var selectColor: Vector4f = new Vector4f(.2f, .2f, 1, 1),
                 protected var cursorColor: Vector4f = Color.black) extends InteractiveGuiBase(x, y, z, maxWidth, maxHeight, true) with Focusable {

  protected var focused = false
  protected var readonly = false

  protected var maxLineLength = math.floor(maxWidth / (.5f * scale)).toInt
  protected var maxLines = math.floor(height / scale).toInt

  if (maxLineLength < 1 || maxLines < 1) Main.error("GuiTextBox", "Given maxWidth and/or height are too small, no characters fit!")

  if (singleLine) maxLines = 1

  protected var text = Localization.process(defaultText) //'text' always contains all of the text!

  protected val lines = (for (i <- 0 until maxLines) yield new TextSprite("TextBox Line " + i, "", scale, textColor, font,
                                                                          xPos, yPos + i * scale, zPos + .001f, selectColor,
                                                                          selectColor.inverse, null, window, scissorBounds)).toArray //'lines' contains sprites which show only visible text
  protected val placementTable = mutable.Map[Int, Int]() //where the cursor can be placed (line -> right most place)
  protected val cursor = new SingleColorSprite("TextBox Cursor", cursorColor, xPos, yPos, zPos, 1, lines(0).getCharHeight, window, scissorBounds) {
    protected var timeCounter = 0d

    override protected def renderSprite(): Unit = {
      timeCounter += Main.frameDeltaTime
      if (timeCounter < .5d) super.renderSprite()
      else if (timeCounter >= 1d) timeCounter = 0d
    }

    override def setPos(x: Float, y: Float, z: Float): Unit = {
      timeCounter = 0d
      super.setPos(x, y, z)
    }
  } //cursor which blinks and resets blinking on placement
  protected var cursorPos = 0 //cursor pos relative to visible text start
  protected var textOffset = 0 //single line visible text offset, if text is longer than maxLineLength
  protected var cursMoveCounter = 0d
  protected var cursMoveSpeed = 1d //cursor continuous movement vars

  protected var selection: Range = _
  protected var selectionStart = 0 //text selection and starting position

  protected var renderLocked = false

  cursor.renderEnabled = false

  createLines()

  keyboardInput = true
  charInput = true

  override def onMouseButton(x: Float, y: Float, button: Int, action: Int): Unit = {
    if (button != GLFW_MOUSE_BUTTON_LEFT || action != GLFW_PRESS || !visible || !enabled) return
    if (!focused) focus()
    placeCursorByPos(x, y, KeyboardHandler.getKey(GLFW_KEY_LEFT_SHIFT))
  }

  override def onMouseMove(x: Float, y: Float): Unit = {
    if (!MouseHandler.getButton(0) || !visible || !enabled) return
    placeCursorByPos(x, y, true)
  }

  override def onKey(key: Int, action: Int, mods: Int): Unit = {
    if (action == GLFW_RELEASE) return //only on press/repeat
    if (key == GLFW_KEY_BACKSPACE && (textOffset + cursorPos > 0 || selection != null) && !readonly) {
      if (selection == null) {
        text = text.substring(0, textOffset + cursorPos - 1) + text.substring(textOffset + cursorPos)
        cursorPos -= 1
      } else deleteSelection() //delete char left of cursor, or selection if present
      createLines()
    } else if (key == GLFW_KEY_ENTER && !readonly) {
      onChar(0xA.toChar, mods) //type newline char
    } else if (key == GLFW_KEY_DELETE && (textOffset + cursorPos < text.length || selection != null) && !readonly) {
      if (selection == null) text = text.substring(0, textOffset + cursorPos) + text.substring(textOffset + cursorPos + 1)
      else deleteSelection() //delete char right of cursor, or selection if present
      createLines()
    } else if (key == GLFW_KEY_LEFT && (textOffset + cursorPos > 0 || (selection != null && mods != GLFW_MOD_SHIFT))) {
      if (selection == null || mods == GLFW_MOD_SHIFT) placeCursor(cursorPos - 1, mods == GLFW_MOD_SHIFT)
      else placeCursor(math.max(selection.start - textOffset, 0)) //move cursor 1 left and select/deselect if needed
    } else if (key == GLFW_KEY_RIGHT && (textOffset + cursorPos < text.length || (selection != null && mods != GLFW_MOD_SHIFT))) {
      if (selection == null || mods == GLFW_MOD_SHIFT) placeCursor(cursorPos + 1, mods == GLFW_MOD_SHIFT)
      else placeCursor(math.min(selection.end - textOffset, maxLineLength)) //move cursor 1 right and select/deselect if needed
    } else if (key == GLFW_KEY_UP && placeFromPos(cursorPos)._1 > 0 && !singleLine) {
      placeCursorByPos(cursor.getX + .001f - xPos, cursor.getY - .1f - yPos, mods == GLFW_MOD_SHIFT) //move cursor 1 up and select/deselect if needed
    } else if (key == GLFW_KEY_DOWN && placeFromPos(cursorPos)._1 < maxLines - 1 && !singleLine) {
      placeCursorByPos(cursor.getX + .001f - xPos, cursor.getY + lines(0).getCharHeight + .1f - yPos, mods == GLFW_MOD_SHIFT) //move cursor 1 down and select/deselect if needed
    } else if (key == GLFW_KEY_C && mods == GLFW_MOD_CONTROL && action == GLFW_PRESS) {
      copySelection()
    } else if (key == GLFW_KEY_X && mods == GLFW_MOD_CONTROL && action == GLFW_PRESS) {
      if (readonly) copySelection() else cutSelection()
    } else if (key == GLFW_KEY_V && mods == GLFW_MOD_CONTROL && action == GLFW_PRESS && !readonly) {
      pasteFromClipboard()
    }
  }

  override def onChar(char: Char, mods: Int): Unit = {
    if (char == 0xA && (singleLine || getAllLines.lengthCompare(maxLines) >= 0)) return //do not allow newline if single line or last line
    if (char == 0xD) return //do not allow CR
    if (readonly) return
    if (selection != null) deleteSelection()
    text = text.substring(0, cursorPos + textOffset) + Character.toString(char) + text.substring(cursorPos + textOffset)
    if (!singleLine && getAllLines.lengthCompare(maxLines) > 0) {
      text = text.substring(0, cursorPos) + text.substring(cursorPos + 1)
    } else cursorPos += 1 //move cursor or abort if multiline and end is reached
    createLines()
  }

  def setSelection(sel: Range, setStart: Boolean = true): Unit = {
    selection = sel
    if (setStart && selection != null) selectionStart = selection.start
    createLines(true)
  }
  def getSelection = selection
  def getSelectedText = text.substring(selection.start, selection.end)

  def deleteSelection(): Unit = {
    if (selection == null) return
    text = text.substring(0, selection.start) + text.substring(selection.end)
    cursorPos = selection.start - textOffset
    if (cursorPos < 0) {
      textOffset += cursorPos
      cursorPos = 0
    }
    setSelection(null, false)
    placeCursor()
  }

  def copySelection(): Unit = {
    if (selection == null) return
    val sel = new StringSelection(text.substring(selection.start, selection.end))
    Toolkit.getDefaultToolkit.getSystemClipboard.setContents(sel, sel) //set system clipboard content to selection
  }

  def cutSelection(): Unit = {
    if (selection == null) return
    copySelection()
    deleteSelection()
  }

  def pasteFromClipboard(): Unit = {
    val obj = Toolkit.getDefaultToolkit.getSystemClipboard.getData(DataFlavor.stringFlavor)
    if (obj == null) return
    val txt = obj.asInstanceOf[String]
    for (c <- txt) onChar(c, 0)
  }

  def createLines(): Unit = createLines(false) //default overload

  protected def createLines(skipCursor: Boolean): Unit = {
    if (!InterThread.isGraphics) {
      InterThread.scheduleGraphics(() => createLines(skipCursor))
      renderLocked = true
      return
    }
    lines.foreach(l => l.setText(""))
    placementTable.clear() //clear all lines and placement table
    if (singleLine) {
      val exChars = text.length - textOffset - maxLineLength //chars to right of line end
      if (exChars < 0) textOffset = math.max(textOffset + exChars, 0) //try lowering text offset if line is not full
      placementTable(0) = math.min(text.length - textOffset, maxLineLength)
      if (exChars < 0 && textOffset > 0) cursorPos = math.min(cursorPos - exChars, placementTable(0)) //move cursor if text offset was lowered, but not to 0
      lines(0).setText(text.substring(textOffset, math.min(textOffset + maxLineLength, text.length)))
      if (selection != null) lines(0).setHighlighted(math.max(selection.start - textOffset, 0)
                                                     until math.max(math.min(selection.end - textOffset, maxLineLength), 0))
      else lines(0).setHighlighted(null) //set line selection if present
    } else {
      val aLines = getAllLines
      val offsets = mutable.MutableList((for (_ <- 0 to maxLines) yield 0): _*) //offsets of line starts in relation to text start, plus one for end
      for (i <- 0 until maxLines) {
        if (i < aLines.length) { //for all existing lines
          lines(i).setText(aLines(i))
          placementTable(i) = aLines(i).length
          offsets(i + 1) = offsets(i) + aLines(i).length + (if (aLines(i).length < maxLineLength) 1 else 0) //next offset, plus one if line is not full (for newline char)
        } else {
          placementTable(i) = -1 //no placement allowed on non-existing lines
        }
      }
      if (selection != null) for (i <- offsets.indices.dropRight(1)) { //for all existing lines if selection exists
        if (offsets(i) < selection.end && offsets(i + 1) > selection.start) { //if line is affected by selection
          val start = math.max(selection.start - offsets(i), 0)
          val end = math.min(selection.end - offsets(i), math.min(maxLineLength, placementTable(i)))
          lines(i).setHighlighted(start until end)
        } else lines(i).setHighlighted(null)
      } else lines.foreach(l => l.setHighlighted(null))
    }
    if (!skipCursor) placeCursor()
    renderLocked = false
  }

  protected def getAllLines: List[String] = {
    val fLines = text.split("\n", -1)
    var aLines = List[String]()
    for (l <- fLines) aLines ++= splitStringAtMax(l)
    aLines
  }

  /**
    * Splits str into lines that are at most maxLineLength long.
    */
  protected def splitStringAtMax(str: String): List[String] = {
    var sps = List[String]()
    if (str.length <= maxLineLength) return List[String](str)
    val x = str.splitAt(maxLineLength)
    sps :+= x._1
    sps ++= splitStringAtMax(x._2)
    sps
  }

  def placeCursor(newPos: Int = -99, selecting: Boolean = false): Unit = {
    if (newPos > -99) cursorPos = newPos
    var (line, column) = placeFromPos(cursorPos)
    if (singleLine && line > 0) {
      textOffset += maxLineLength * (line - 1) + column + 1
      line = 0
      column = maxLineLength
      cursorPos = maxLineLength
      createLines(true)
    } else if (singleLine && cursorPos < 0 && textOffset > 0) {
      textOffset += cursorPos
      cursorPos = 0
      createLines(true)
    }
    if (selecting) {
      if (selectionStart < cursorPos + textOffset) setSelection(selectionStart until cursorPos + textOffset, false)
      else setSelection(cursorPos + textOffset until selectionStart, false)
    } else if (selection != null && newPos > -99) {
      setSelection(null, false)
    }
    if (selection == null) selectionStart = cursorPos + textOffset
    if (singleLine && ((cursorPos == 0 && textOffset > 0) || (cursorPos == maxLineLength && textOffset + cursorPos < text.length))) updateEnabled = true
    else { updateEnabled = false; cursMoveSpeed = 1; cursMoveCounter = 0 }
    cursor.setPos(xPos + column * lines(0).getCharWidth, yPos + line * lines(0).getCharHeight, zPos)
  }

  def placeCursorByPos(x: Float, y: Float, selecting: Boolean = false): Unit = {
    // get clicked line and column based on char size, clamp to legal values
    val clickedLine = math.min(math.max(math.floor(y / lines(0).getCharHeight), 0), maxLines - 1).toInt
    val clickedColumn = math.min(math.max(math.round(x / lines(0).getCharWidth), 0), maxLineLength)

    if (renderLocked) return

    if (placementTable(clickedLine) < 0) {
      placeCursorByPos(maxLineLength * lines(0).getCharWidth - .01f, y - lines(0).getCharHeight, selecting)
      return
    }
    val column = math.min(clickedColumn, placementTable(clickedLine))
    cursorPos = posFromPlace(clickedLine, column)
    if (selecting) {
      if (selectionStart < cursorPos + textOffset) setSelection(selectionStart until cursorPos + textOffset, false)
      else setSelection(cursorPos + textOffset until selectionStart, false)
    } else if (selection != null) {
      setSelection(null, false)
    }
    if (selection == null) selectionStart = cursorPos + textOffset
    if (singleLine && ((cursorPos == 0 && textOffset > 0) || (cursorPos == maxLineLength && textOffset + cursorPos < text.length))) updateEnabled = true
    else { updateEnabled = false; cursMoveSpeed = 1; cursMoveCounter = 0 }
    cursor.setPos(xPos + column * lines(0).getCharWidth, yPos + clickedLine * lines(0).getCharHeight, zPos)
  }

  protected def posFromPlace(line: Int, column: Int): Int = {
    var pos = 0
    for (i <- 0 until line) {
      if (placementTable(i) == maxLineLength) pos += maxLineLength
      else {
        pos += placementTable(i)
        if (text.charAt(pos) == 0xA) pos += 1
      }
    }
    pos + column
  }

  protected def placeFromPos(pos: Int): (Int, Int) = {
    var line, column = 0
    for (i <- 0 until pos) {
      column += 1
      if ((text.charAt(i) == 0xA && !singleLine) || (column >= maxLineLength && !singleLine) || (column > maxLineLength && singleLine)) {
        column = 0
        line += 1
      }
    }
    if (!singleLine && line >= maxLines) {
      line = maxLines - 1
      column = maxLineLength
    }
    (line, column)
  }

  /**
    * Update only used for cursor movement
    */
  override def update(): Unit = {
    super.update()
    if (!MouseHandler.getButton(0)) { updateEnabled = false; cursMoveSpeed = 1; cursMoveCounter = 0; return }
    cursMoveCounter += Main.updateDeltaTime
    if (cursMoveCounter >= .5 * cursMoveSpeed) {
      if (cursorPos == 0) placeCursor(-1, true)
      else if (cursorPos == maxLineLength) placeCursor(maxLineLength + 1, true)
      cursMoveCounter = 0
      cursMoveSpeed *= .75
    }
  }

  override protected def onScissorSet(): Unit = {
    lines.foreach(l => l.setScissorBounds(scissorBounds))
    cursor.setScissorBounds(scissorBounds)
  }

  override protected def onWindowSet(): Unit = {
    lines.foreach(l => l.setWindow(window))
    cursor.setWindow(window)
  }

  override def setPos(x: Float, y: Float, z: Float): Unit = {
    val dx = x - xPos
    val dy = y - yPos
    val dz = z - zPos
    super.setPos(x, y, z)
    lines.foreach(l => l.setPos(l.getX + dx, l.getY + dy, l.getZ + dz))
    cursor.setPos(cursor.getX + dx, cursor.getY + dy, cursor.getZ + dz)
  }

  override protected def onPosChanged(): Unit = {}

  /**
    * Sets the entire text of this TextBox.
    * @param txt New text, localization placeholders will be resolved.
    */
  def setText(txt: String): Unit = {
    text = Localization.process(txt)
    val aLines = getAllLines
    if (aLines.lengthCompare(maxLines) > 0) {
      text = ""
      for (i <- 0 until maxLines) text += aLines(i) + (if (i < maxLines - 1 && aLines(i).length < maxLineLength) "\n" else "")
    }
    setSelection(null)
  }
  def getText = text

  override def setColor(color: Vector4f): Unit = {
    textColor = color
    lines.foreach(l => l.setColor(textColor))
  }
  override def getColor: Vector4f = new Vector4f(textColor)

  def setSelectColor(color: Vector4f): Unit = {
    selectColor = color
    lines.foreach(l => {
      l.setHighColor(selectColor)
      l.setHighTextColor(selectColor.inverse)
    })
  }
  def getSelectColor: Vector4f = new Vector4f(selectColor)

  override def setEnabled(enable: Boolean): Unit = {
    super.setEnabled(enable)
    if (!enable && focused) Focusable.removeFocus()
  }

  def setReadOnly(): Unit = readonly = true
  def setReadWrite(): Unit = readonly = false
  def isReadOnly: Boolean = readonly

  override def onGainFocus(): Unit = {
    focused = true
    cursor.renderEnabled = true
  }

  override def onLoseFocus(): Unit = {
    focused = false
    cursor.renderEnabled = false
    setSelection(null)
  }

  override protected def onHide(): Unit = {
    lines.foreach(l => l.renderEnabled = false)
    if (focused) Focusable.removeFocus()
  }

  override protected def onShow(): Unit = lines.foreach(l => l.renderEnabled = true)

  override protected def onDispose(): Unit = {
    lines.foreach(l => l.dispose())
    cursor.dispose()
  }

  override def getWidth: Float = lines(0).getCharWidth * maxLineLength
  override def getHeight: Float = lines(0).getCharHeight * maxLines
  override def getDepth: Float = cursor.getDepth + .001f + lines(0).getDepth
}
