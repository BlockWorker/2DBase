package com.blockworker.base2d.gui

import com.blockworker.base2d.core.Event
import com.blockworker.base2d.input.MouseHandler
import com.blockworker.base2d.sprite.ScreenObject
import org.lwjgl.glfw.GLFW._

/**
  * Simple seek bar implementation with no sprites by default.
  */
abstract class GuiSeekBar(x: Float, y: Float, z: Float, wid: Float, hei: Float, protected var minValue: Int,
                          protected var maxValue: Int, val horizontal: Boolean = false) extends InteractiveGuiBase(x, y, z, wid, hei, true) {

  protected var value = 0
  protected var lastMouseCoords = (0f, 0f)
  protected var dragging = false

  val ValueChanged = new Event[Int]

  override def onMouseButton(x: Float, y: Float, button: Int, action: Int): Unit = {
    if (button != GLFW_MOUSE_BUTTON_LEFT && button != GLFW_MOUSE_BUTTON_RIGHT) return
    if (action != GLFW_PRESS) {
      dragging = !MouseHandler.getButton(GLFW_MOUSE_BUTTON_LEFT) && !MouseHandler.getButton(GLFW_MOUSE_BUTTON_RIGHT)
      return
    }
    if (getSeekSliderBounds.isInRegion(x, y)) {
      lastMouseCoords = (x, y)
      dragging = true
      return
    }
    if (horizontal) {
      if (inClickArea(x, y) && x < getSeekSliderBounds.getX && value > minValue) value -= 1
      else if (inClickArea(x, y) && x > getSeekSliderBounds.getX + getSeekSliderBounds.getWidth && value < maxValue) value += 1
      else return
    } else {
      if (inClickArea(x, y) && y < getSeekSliderBounds.getY && value > minValue) value -= 1
      else if (inClickArea(x, y) && y > getSeekSliderBounds.getY + getSeekSliderBounds.getHeight && value < maxValue) value += 1
      else return
    }
    onValueChanged()
  }

  override def onMouseMove(x: Float, y: Float): Unit = {
    if (!dragging) return
    val offset = if (horizontal) x - lastMouseCoords._1 else y - lastMouseCoords._2
    lastMouseCoords = (x, y)
    val minOffset = (if (horizontal) width else height) / (2 * (maxValue - minValue) - 2)
    if (value > minValue && offset <= -minOffset) value -= 1
    else if (value < maxValue && offset >= minOffset) value += 1
    else return
    onValueChanged()
  }

  protected def inClickArea(x: Float, y: Float): Boolean
  protected def getSeekSliderBounds: ScreenObject

  def setValue(newValue: Int): Unit = {
    if (newValue < minValue || newValue > maxValue) return
    value = newValue
    onValueChanged()
  }
  def getValue = value

  protected def onValueChanged(): Unit = {
    updateVisual()
    ValueChanged(this, value)
  }

  protected def updateVisual(): Unit

  def setMinValue(min: Int): Unit = {
    minValue = min
    updateVisual()
  }
  def getMinValue = minValue

  def setMaxValue(max: Int): Unit = {
    maxValue = max
    updateVisual()
  }
  def getMaxValue = maxValue

}
