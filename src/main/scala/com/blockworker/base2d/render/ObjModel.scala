package com.blockworker.base2d.render

import java.nio.charset.StandardCharsets
import java.nio.file._

import com.blockworker.base2d.core.InterThread
import org.joml.{Vector3f, Vector4f}
import org.lwjgl.BufferUtils
import org.lwjgl.opengl.GL11._
import org.lwjgl.opengl.GL15._
import org.lwjgl.opengl.GL20._
import org.lwjgl.opengl.GL30._

import scala.collection.mutable

import ObjModel._

/**
  * Created by Alex on 12.01.2017.
  */
object ObjModel {

  val vRegex = """(?m)^v +(-?\d+\.\d+) (-?\d+\.\d+) (-?\d+\.\d+)""".r
  val vtRegex = """(?m)^vt +(-?\d+\.\d+) (-?\d+\.\d+) (-?\d+\.\d+)""".r
  val gRegex = """(?m)^g (.+)""".r
  val fRegex = """(?m)^f +(\d+)/(\d+) (\d+)/(\d+) (\d+)/(\d+)""".r
  val colRegex = """(.+)#([0-9A-F]{8})""".r

  val loadedModels = mutable.Map[(String, Float), mutable.Map[String, Group]]()

  def getModelPath(domain: String, modPath: String): String = "res/" + domain + "/models/" + modPath

  def unloadAll(): Unit = loadedModels.clear()

}

class ObjModel(val path: String, protected var scale: Float, protected var defaultColor: Vector4f) {

  protected var relativeXRange: (Float, Float) = _
  protected var relativeYRange: (Float, Float) = _
  protected var relativeZRange: (Float, Float) = _

  var groups = mutable.Map[String, Group]()

  load()

  def load(): Unit = {
    if (loadedModels.isDefinedAt((path, scale))) {
      val mod = loadedModels((path, scale))
      groups = mod.clone()
      groups.filter(g => !g._2.isColorOverridden).foreach(i => i._2.setColor(defaultColor))
      return
    }

    groups.clear()
    val file = new String(Files.readAllBytes(FileSystems.getDefault.getPath(path)), StandardCharsets.UTF_8)
    val v = (for (ve <- vRegex.findAllMatchIn(file)) yield Array[Float](ve.group(1).toFloat * scale, ve.group(2).toFloat * scale, ve.group(3).toFloat * scale)).toArray
    relativeXRange = (v.minBy(a => a(0)).apply(0), v.maxBy(a => a(0)).apply(0))
    relativeYRange = (v.minBy(a => a(1)).apply(1), v.maxBy(a => a(1)).apply(1))
    relativeZRange = (v.minBy(a => a(2)).apply(2), v.maxBy(a => a(2)).apply(2))
    val vt = (for (vet <- vtRegex.findAllMatchIn(file)) yield Array[Float](vet.group(1).toFloat, vet.group(2).toFloat)).toArray
    var gn = ""
    var verts = Array[Float]()
    file.split('\n').foreach { l =>
      val g = gRegex.findFirstMatchIn(l)
      if (g.nonEmpty) {
        if (gn != "") {
          val colMatch = colRegex.findFirstMatchIn(gn)
          if (colMatch.nonEmpty) {
            val r = Integer.parseInt(colMatch.get.group(2).substring(0, 2), 16)
            val g = Integer.parseInt(colMatch.get.group(2).substring(2, 4), 16)
            val b = Integer.parseInt(colMatch.get.group(2).substring(4, 6), 16)
            val a = Integer.parseInt(colMatch.get.group(2).substring(6), 16)
            groups(colMatch.get.group(1)) = new Group(colMatch.get.group(1), verts.clone(), new Vector4f(r / 255f, g / 255f, b / 255f, a / 255f))
          } else groups(gn) = new Group(gn, verts.clone(), defaultColor)
        }
        verts = Array[Float]()
        gn = g.get.group(1)
      }
      val f = fRegex.findFirstMatchIn(l)
      if (f.nonEmpty) {
        verts ++= v(f.get.group(1).toInt - 1)
        verts ++= vt(f.get.group(2).toInt - 1)
        verts ++= v(f.get.group(3).toInt - 1)
        verts ++= vt(f.get.group(4).toInt - 1)
        verts ++= v(f.get.group(5).toInt - 1)
        verts ++= vt(f.get.group(6).toInt - 1)
      }
    }
    if (gn != "") {
      val colMatch = colRegex.findFirstMatchIn(gn)
      if (colMatch.nonEmpty) {
        val r = Integer.parseInt(colMatch.get.group(2).substring(0, 2), 16)
        val g = Integer.parseInt(colMatch.get.group(2).substring(2, 4), 16)
        val b = Integer.parseInt(colMatch.get.group(2).substring(4, 6), 16)
        val a = Integer.parseInt(colMatch.get.group(2).substring(6), 16)
        groups(colMatch.get.group(1)) = new Group(colMatch.get.group(1), verts.clone(), new Vector4f(r / 255f, g / 255f, b / 255f, a / 255f))
      } else groups(gn) = new Group(gn, verts.clone(), defaultColor)
    }

    loadedModels((path, scale)) = groups.clone()

    initAll()
  }

  def initAll(): Unit = {
    if (!InterThread.isGraphics) {
      InterThread.scheduleGraphics(() => initAll())
      return
    }
    groups.foreach(g => g._2.init())
  }

  def drawAll(x: Float, y: Float, z: Float): Unit = {
    if (!InterThread.isGraphics) {
      InterThread.scheduleGraphics(() => drawAll(x, y, z))
      return
    }

    RenderUtils.modelMatrix.translation(x, y, z)
    RenderUtils.pushModelMatrix()

    for ((_, g) <- groups) {
      glBindVertexArray(g.getVAO)
      glDrawArrays(GL_TRIANGLES, 0, g.getVertCount)
    }

    glBindVertexArray(0)

    RenderUtils.modelMatrix.identity()
    RenderUtils.pushModelMatrix()

    RenderUtils.cancelOnGLError("ObjModel", "drawAll")
  }

  def multiDraw(draws: (String, Vector3f)*): Unit = {
    if (!InterThread.isGraphics) {
      InterThread.scheduleGraphics(() => multiDraw(draws: _*))
      return
    }

    for ((i, p) <- draws.filter(d => groups.isDefinedAt(d._1))) {
      RenderUtils.modelMatrix.translation(p)
      RenderUtils.pushModelMatrix()
      glBindVertexArray(groups(i).getVAO)
      glDrawArrays(GL_TRIANGLES, 0, groups(i).getVertCount)
    }

    glBindVertexArray(0)

    RenderUtils.modelMatrix.identity()
    RenderUtils.pushModelMatrix()

    RenderUtils.cancelOnGLError("ObjModel", "multiDraw")
  }

  def setScale(scl: Float): Unit = {
    scale = scl
    load()
  }
  def getScale = scale

  def setDefaultColor(clr: Vector4f): Unit = {
    defaultColor = clr
    groups.filter(g => !g._2.isColorOverridden).foreach(i => i._2.setColor(clr))
  }
  def getDefaultColor = defaultColor

  def setColors(colors: Map[String, Vector4f]): Unit = for ((i, c) <- colors.filter(e => groups.isDefinedAt(e._1))) {
    groups(i).setColor(c)
    groups(i).setColorOverridden(true)
  }

  def resetToDefault(names: String*): Unit = {
    names.filter(e => groups.isDefinedAt(e)).foreach { i =>
      groups(i).setColor(defaultColor)
      groups(i).setColorOverridden(false)
    }
  }

  def dispose(): Unit = {
    groups.foreach(g => g._2.dispose())
    groups.clear()
  }

  def getRelXRange = relativeXRange
  def getRelYRange = relativeYRange
  def getRelZRange = relativeZRange
  def getWidth = relativeXRange._2 - relativeXRange._1
  def getHeight = relativeYRange._2 - relativeYRange._1
  def getDepth = relativeZRange._2 - relativeZRange._1

}

class Group(val name: String, protected val relVerts: Array[Float], protected var color: Vector4f,
            protected var colorOverridden: Boolean = false) {

  protected var vaoId = 0
  protected var vboId = 0
  protected var vertCount = 0

  def init(): Unit = {
    if (!InterThread.isGraphics) {
      InterThread.scheduleGraphics(() => init())
      return
    }

    var vertices = Array[Float]()
    for (i <- 0 until (relVerts.length, 5)) {
      vertices ++= relVerts.slice(i, i + 3)
      vertices ++= Array[Float](1, 1, 1, 1, 1)
      vertices ++= relVerts.slice(i + 3, i + 5)
    }
    vertCount = vertices.length / 10
    val vertBuffer = BufferUtils.createFloatBuffer(vertices.length)
    vertBuffer.put(vertices)
    vertBuffer.flip()

    if (vaoId != 0) {
      glDisableVertexAttribArray(2)
      glDisableVertexAttribArray(1)
      glDisableVertexAttribArray(0)
      glDeleteVertexArrays(vaoId)
    }
    if (vboId != 0) glDeleteBuffers(vboId)

    vaoId = glGenVertexArrays()
    glBindVertexArray(vaoId)

    vboId = glGenBuffers()
    glBindBuffer(GL_ARRAY_BUFFER, vboId)
    glBufferData(GL_ARRAY_BUFFER, vertBuffer, GL_STATIC_DRAW)
    glVertexAttribPointer(0, 4, GL_FLOAT, false, 40, 0)
    glVertexAttribPointer(1, 4, GL_FLOAT, false, 40, 16)
    glVertexAttribPointer(2, 2, GL_FLOAT, false, 40, 32)
    glBindBuffer(GL_ARRAY_BUFFER, 0)

    glEnableVertexAttribArray(0)
    glEnableVertexAttribArray(1)
    glEnableVertexAttribArray(2)

    glBindVertexArray(0)
    RenderUtils.cancelOnGLError("Group", "init")
  }

  def draw(x: Float, y: Float, z: Float): Unit = {
    if (!InterThread.isGraphics) {
      InterThread.scheduleGraphics(() => draw(x, y, z))
      return
    }

    if (!glIsVertexArray(vaoId) || !glIsBuffer(vboId)) return

    RenderUtils.modelMatrix.translation(x, y, z)
    RenderUtils.pushModelMatrix()

    RenderUtils.setModColor(color)

    glBindVertexArray(vaoId)

    glDrawArrays(GL_TRIANGLES, 0, vertCount)

    glBindVertexArray(0)

    RenderUtils.resetModColor()

    RenderUtils.modelMatrix.identity()
    RenderUtils.pushModelMatrix()

    RenderUtils.cancelOnGLError("Group", "draw")
  }

  def setColor(clr: Vector4f) = {
    color = clr
  }
  def getColor = color

  def dispose(): Unit = {
    if (!InterThread.isGraphics) {
      InterThread.scheduleGraphics(() => dispose())
      return
    }
    if (vaoId != 0) {
      glDisableVertexAttribArray(2)
      glDisableVertexAttribArray(1)
      glDisableVertexAttribArray(0)
      glDeleteVertexArrays(vaoId)
    }
    if (vboId != 0) glDeleteBuffers(vboId)
  }

  def getVAO = vaoId
  def getVertCount = vertCount
  def setColorOverridden(overridden: Boolean) = colorOverridden = overridden
  def isColorOverridden = colorOverridden

}