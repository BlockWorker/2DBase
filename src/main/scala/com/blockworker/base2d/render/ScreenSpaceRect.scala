package com.blockworker.base2d.render

import com.blockworker.base2d.Main
import com.blockworker.base2d.core.BaseSettings

object ScreenSpaceRect {

  def getPixelsPerXCoordinate: Double = Main.letterboxedWidth.toDouble / BaseSettings.coordRes().width.toDouble
  def getPixelsPerYCoordinate: Double = Main.letterboxedHeight.toDouble / BaseSettings.coordRes().height.toDouble

  def rectFromCoords(x: Float, y: Float, width: Float, height: Float): ScreenSpaceRect =
    new ScreenSpaceRect((x * getPixelsPerXCoordinate).toInt + Main.letterboxLeft, ((if (Main.topToBottom) -y - height else y) * getPixelsPerYCoordinate).toInt + (if (Main.topToBottom) Main.letterboxedHeight + Main.letterboxBottom else Main.letterboxBottom),
                  (width * getPixelsPerXCoordinate).toInt, (height * getPixelsPerYCoordinate).toInt)

}

/**
  * Represents a rectangle of pixels in screen space.
  */
class ScreenSpaceRect(val x: Int, val y: Int, val width: Int, val height: Int) {

  def getCoordinateBounds: (Float, Float, Float, Float) =
    if (Main.topToBottom)
      (((x - Main.letterboxLeft) / ScreenSpaceRect.getPixelsPerXCoordinate).toFloat, (-(y - Main.letterboxBottom - Main.letterboxedHeight) / ScreenSpaceRect.getPixelsPerYCoordinate).toFloat,
        (width / ScreenSpaceRect.getPixelsPerXCoordinate).toFloat, (height / ScreenSpaceRect.getPixelsPerYCoordinate).toFloat)
    else
      (((x - Main.letterboxLeft) / ScreenSpaceRect.getPixelsPerXCoordinate).toFloat, ((y - Main.letterboxBottom) / ScreenSpaceRect.getPixelsPerYCoordinate).toFloat,
        (width / ScreenSpaceRect.getPixelsPerXCoordinate).toFloat, (height / ScreenSpaceRect.getPixelsPerYCoordinate).toFloat)

}
