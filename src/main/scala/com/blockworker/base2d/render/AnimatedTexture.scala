package com.blockworker.base2d.render

import com.blockworker.base2d.Main
import com.blockworker.base2d.core.InterThread
import com.blockworker.base2d.core.updating.{FrameUpdating, UpdateManager}
import org.lwjgl.opengl.GL11._
import org.lwjgl.opengl.GL13._

/**
  * Created by Alex on 18.09.2016.
  */
class AnimatedTexture(protected var texLoc: String, protected var texId: Int, protected var frames: Int, protected var columns: Int,
                      protected var rows: Int, protected var fps: Double, protected var width: Int, protected var height: Int) extends FrameUpdating with Texture {

  protected val columnWidth = 1f / columns.toFloat
  protected val rowHeight = 1f / rows.toFloat
  protected var frametime = 1d / fps

  protected var minU = 0f
  protected var maxU = columnWidth
  protected var minV = 0f
  protected var maxV = rowHeight

  protected var progress = 0d
  protected var frame = 0
  protected var update = false
  protected var reverse = false

  /**
    * Binds the texture for use in rendering calls.
    * @note This will override any currently bound texture.
    */
  override def bind(): Unit = {
    if (!InterThread.isGraphics) {
      InterThread.scheduleGraphics(() => bind())
      return
    }

    if (!glIsTexture(texId)) {
      reload()
      println("Reloaded Texture " + texLoc)
    }
    glActiveTexture(GL_TEXTURE0)
    glBindTexture(GL_TEXTURE_2D, texId)
  }

  /**
    * Unbinds the texture.
    * @note This will unbind any texture that is currently bound, so watch out!
    */
  override def unbind(): Unit = {
    RenderUtils.bindDefaultTexture()
  }

  /**
    * Called every frameInterval frames if this is registered in [[com.blockworker.base2d.core.updating.UpdateManager UpdateManager]] and renderEnabled is true.
    */
  override def render(): Unit = {
    if (!InterThread.isGraphics) {
      InterThread.scheduleGraphics(() => render())
      return
    }

    update = false

    progress += Main.frameDeltaTime
    val advFrames = math.floor(progress / frametime)
    if (advFrames == 0) return

    progress -= advFrames * frametime
    if (reverse) frame -= advFrames.toInt
    else frame += advFrames.toInt
    while (frame >= frames) frame -= frames
    while (frame < 0) frame += frames
    minU = (frame % columns) * columnWidth
    maxU = ((frame % columns) + 1) * columnWidth
    minV = math.floor(frame.toDouble / columns.toDouble).toInt * rowHeight
    maxV = (math.floor(frame.toDouble / columns.toDouble) + 1).toInt * rowHeight

    update = true
  }

  def pause(): Unit = renderEnabled = false
  def resume(): Unit = renderEnabled = true

  def gotoFrame(_frame: Int): Unit = {
    frame = 0
    progress = (_frame + .001d) * frametime
    render()
  }

  def setFPS(_fps: Double): Unit = {
    if (_fps < 0) {
      fps = -_fps
      reverse = true
    } else {
      fps = _fps
      reverse = false
    }
    frametime = 1d / fps
  }

  def setReverse(_reverse: Boolean): Unit = reverse = _reverse

  override def reload(): Unit = {
    texId = RenderUtils.getTextureId(texLoc, true)
  }

  def dispose(): Unit = {
    UpdateManager.unregisterFrameUpdate(this)
  }

  override def setPos(x: Float, y: Float, z: Float): Unit = {}

  override def getTexLoc: String = texLoc
  override def getTexId: Int = texId
  override def getMinU: Float = minU
  override def getMaxU: Float = maxU
  override def getMinV: Float = minV
  override def getMaxV: Float = maxV
  override def updated: Boolean = update
  override def getTexWidth: Int = width
  override def getTexHeight: Int = height
  override def getX: Float = 0
  override def getY: Float = 0
  override def getZ: Float = 300
  override def getWidth: Float = getTexWidth
  override def getHeight: Float = getTexHeight

  override def setWindow(window: Long): Unit = {}
  override def getWindow: Long = Main.windows.head
}
