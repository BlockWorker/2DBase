package com.blockworker.base2d.render



import com.blockworker.base2d.core.updating.FrameUpdating
import org.lwjgl.opengl.GL20

import scala.collection.mutable

class ShaderProgram(protected var shaders: mutable.Set[Shader], protected var attribLocs: mutable.MutableList[String],
                    private val _uniforms: Set[String]) extends FrameUpdating {

  protected var programID: Int = -1
  protected var uniforms = mutable.Map.empty[String, Int]
  renderEnabled = false

  initProgram()
  for (uniform <- _uniforms) uniforms(uniform) = -1

  def this() {
    this(mutable.Set.empty[Shader], mutable.MutableList.empty[String], Set.empty[String])
  }

  def initProgram(): Unit = {
    programID = GL20.glCreateProgram()
  }

  def compileProgram(): Unit = {
    for (s <- shaders) GL20.glAttachShader(programID, s.getShaderID)

    for ((attr, i) <- attribLocs.zipWithIndex) GL20.glBindAttribLocation(programID, i, attr)

    GL20.glLinkProgram(programID)
    GL20.glValidateProgram(programID)

    for (uniform <- uniforms.keys)
      uniforms(uniform) = GL20.glGetUniformLocation(programID, uniform)

    RenderUtils.cancelOnGLError("ShaderProgram", "compileProgram")
  }

  def addShader(shader: Shader): Unit = {
    if (!shaders.exists(s => s.shaderType == shader.shaderType)) shaders.add(shader)
  }

  def removeShader(shader: Shader): Unit = shaders.remove(shader)

  def removeShader(shaderType: Int): Unit = shaders = shaders.filter(s => s.shaderType != shaderType)

  def addAttrib(attr: String, id: Int): Unit = {
    if (!attribLocs.isDefinedAt(id)) attribLocs :+= attr
  }

  def addUniform(uniform: String): Unit = {
    if (!uniforms.contains(uniform)) uniforms(uniform) = -1
  }

  def removeUniform(uniform: String): Unit = uniforms.remove(uniform)

  def getUniformLoc(uniform: String) = if (uniforms.isDefinedAt(uniform)) uniforms(uniform) else -1

  def useProgram(): Unit = GL20.glUseProgram(programID)

  def getProgramID = programID

  protected def updateUniforms(): Unit = {}

  def startUniformUpdates(): Unit = renderEnabled = true

  def stopUniformUpdates(): Unit = renderEnabled = false

  /**
    * Called every frame if this is registered in [[com.blockworker.base2d.core.updating.UpdateManager UpdateManager]] and renderEnabled is true.
    */
  override def render(): Unit = updateUniforms()

  override def setPos(x: Float, y: Float, z: Float): Unit = {}

  override def setWindow(window: Long): Unit = {}

  override def getWindow = 0

  override def getX = 0

  override def getY = 0

  override def getZ = 301

  override def getWidth = 0

  override def getHeight = 0
}
