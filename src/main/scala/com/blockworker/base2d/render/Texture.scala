package com.blockworker.base2d.render

/**
  * Defines a texture object.
  */
trait Texture {

  /**
    * Binds the texture for use in rendering calls.
    * @note This will override any currently bound texture.
    */
  def bind(): Unit

  /**
    * Unbinds the texture.
    * @note This will unbind any texture that is currently bound, so watch out!
    */
  def unbind(): Unit

  def reload(): Unit

  def getTexLoc: String
  def getTexId: Int
  def getMinU: Float
  def getMaxU: Float
  def getMinV: Float
  def getMaxV: Float
  def getTexWidth: Int
  def getTexHeight: Int
  def updated: Boolean

}
