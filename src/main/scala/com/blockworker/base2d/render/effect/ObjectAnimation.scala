package com.blockworker.base2d.render.effect

import com.blockworker.base2d.Main
import com.blockworker.base2d.core.Event
import com.blockworker.base2d.core.updating.{FrameUpdating, UpdateManager}
import com.blockworker.base2d.sprite.ScreenObject
import org.joml.{Vector3f, Vector4f}

import scala.collection.mutable

/**
  * Defines an object based animation which supports object movement and color/alpha changes.
  */
abstract class ObjectAnimation {

  val adjPos = mutable.Map[ScreenObject, Vector3f]()

  /**
    * Applies and starts this animation on the supplied objects.
    * @param objects Objects to apply the animation to.
    * @return Created animation instance or null if objects are invalid
    */
  def start(objects: ScreenObject*): ObjectAnimationInstance = {
    if (!areObjectsValid(objects)) return null
    val ret = new ObjectAnimationInstance(this, makeTasks(objects), false)
    adjPos.clear()
    ret
  }

  /**
    * Applies this animation to the supplied objects. Animation must be progressed manually.
    * @param objects Objects to apply the animation to.
    * @return Created animation instance or null if objects are invalid
    */
  def create(objects: ScreenObject*): ObjectAnimationInstance = {
    if (!areObjectsValid(objects)) return null
    val ret = new ObjectAnimationInstance(this, makeTasks(objects), true)
    adjPos.clear()
    ret
  }

  protected def move(obj: ScreenObject, x: Float, y: Float, z: Float, start: Long, end: Long = -1): (ScreenObject, Move, (Long, Long)) = {
    val endTime = math.max(start, end)
    (obj, new Move(new Vector3f(x, y, z), obj.getPos), (start, endTime))
  }

  protected def moveRelative(obj: ScreenObject, dx: Float, dy: Float, dz: Float, start: Long, end: Long = -1): (ScreenObject, Move, (Long, Long)) = {
    val endTime = math.max(start, end)
    if (!adjPos.isDefinedAt(obj)) adjPos(obj) = obj.getPos
    val st = new Vector3f(adjPos(obj))
    val dest = new Vector3f(adjPos(obj).add(dx, dy, dz))
    (obj, new Move(dest, st), (start, endTime))
  }

  protected def changeColor(obj: ScreenObject, color: Vector4f, start: Long, end: Long = -1): (ScreenObject, Color, (Long, Long)) = {
    val endTime = math.max(start, end)
    (obj, new Color(new Vector4f(color), obj.getColor), (start, endTime))
  }

  protected def changeAlpha(obj: ScreenObject, alpha: Float, start: Long, end: Long = -1): (ScreenObject, Alpha, (Long, Long)) = {
    val endTime = math.max(start, end)
    (obj, new Alpha(alpha, obj.getColor.w), (start, endTime))
  }

  protected def setVisibility(obj: ScreenObject, visible: Boolean, time: Long): (ScreenObject, Visibility, (Long, Long)) =
    (obj, new Visibility(visible), (time, time))

  /**
    * @param func Function to execute at the given time. Params: given ScreenObject, actual time of execution
    */
  protected def execute(obj: ScreenObject, func: (ScreenObject, Long) => Any, time: Long): (ScreenObject, Execute, (Long, Long)) =
    (obj, new Execute(func), (time, time))

  /**
    * @return Set of instructions formatted (affected object, task to perform on object, (start time, end time)) with start time <= end time.
    * @note It is advised to use the existing `move`, `moveRelative`, `changeColor`, `changeAlpha`, `setVisibility` and `execute` methods to generate elements.
    */
  protected def makeTasks(objects: Seq[ScreenObject]): mutable.Set[(ScreenObject, Task[_], (Long, Long))]

  protected def areObjectsValid(objects: Seq[ScreenObject]): Boolean
}

class ObjectAnimationInstance(val animation: ObjectAnimation, val tasks: mutable.Set[(ScreenObject, Task[_], (Long, Long))],
                                        protected var manual: Boolean) extends FrameUpdating {

  if (manual) renderEnabled = false

  val startTime = System.currentTimeMillis()

  val Finished = new Event[Long]

  override def render(): Unit = {
    progress(System.currentTimeMillis() - startTime)
  }

  def progress(curTime: Long): Unit = {
    val progressTasks = tasks.filter(t => t._3._1 <= curTime && t._3._2 > curTime)
    val finTasks = tasks.filter(t => t._3._2 <= curTime).toSeq.sortWith((a, b) => a._3._2 < b._3._2)
    tasks --= finTasks
    for (t <- progressTasks) {
      val progress = (curTime - t._3._1).toDouble / (t._3._2 - t._3._1).toDouble
      t._2 match {
        case m: Move => t._1.setPos(m.interp(progress))
        case c: Color => t._1.setColor(c.interp(progress))
        case a: Alpha =>
          val color = t._1.getColor
          color.w = a.interp(progress)
          t._1.setColor(color)
        case _ =>
      }
    }
    for (t <- finTasks) {
      t._2 match {
        case m: Move => t._1.setPos(m.data)
        case c: Color => t._1.setColor(c.data)
        case a: Alpha =>
          val color = t._1.getColor
          color.w = a.data
          t._1.setColor(color)
        case v: Visibility => if (v.data) t._1.show() else t._1.hide()
        case e: Execute => e.data(t._1, curTime)
      }
    }
    if (isFinished) {
      Finished(this, curTime)
      dispose()
    }
  }

  def setManualProgress(value: Boolean) = {
    manual = value
    renderEnabled = !manual
  }

  def forceFinish(): Unit = {
    val finTime = System.currentTimeMillis() - startTime
    for (t <- tasks.toSeq.sortWith((a, b) => a._3._2 < b._3._2)) {
      t._2 match {
        case m: Move => t._1.setPos(m.data)
        case c: Color => t._1.setColor(c.data)
        case a: Alpha =>
          val color = t._1.getColor
          color.w = a.data
          t._1.setColor(color)
        case v: Visibility => if (v.data) t._1.show() else t._1.hide()
        case e: Execute => e.data(t._1, finTime)
      }
    }
    Finished(this, finTime)
    dispose()
  }

  def isFinished = tasks.isEmpty

  def dispose(): Unit = {
    tasks.clear()
    UpdateManager.unregisterFrameUpdate(this)
  }

  override def setPos(x: Float, y: Float, z: Float): Unit = {}
  override def getX: Float = 0
  override def getY: Float = 0
  override def getZ: Float = 300
  override def getWidth: Float = 0
  override def getHeight: Float = 0

  override def setWindow(window: Long): Unit = {}

  override def getWindow: Long = 0
}

abstract class Task[T] {
  val data: T
  def interp(progress: Double): T
}
class Move(override val data: Vector3f, val start: Vector3f = null) extends Task[Vector3f] {
  override def interp(progress: Double): Vector3f = {
    if (start == null) return data
    val dc = new Vector3f(data)
    dc.sub(start).mul(progress.toFloat).add(start)
  }
}
class Color(override val data: Vector4f, val start: Vector4f = null) extends Task[Vector4f] {
  override def interp(progress: Double): Vector4f = {
    if (start == null) return data
    val dc = new Vector4f(data)
    dc.sub(start).mul(progress.toFloat).add(start)
  }
}
class Alpha(override val data: Float, val start: Float = -1) extends Task[Float] {
  override def interp(progress: Double): Float = {
    if (start < 0) data
    else (data - start) * progress.toFloat + start
  }
}
class Visibility(override val data: Boolean) extends Task[Boolean] {
  override def interp(progress: Double): Boolean = data
}
class Execute(override val data: (ScreenObject, Long) => Any) extends Task[(ScreenObject, Long) => Any] {
  override def interp(progress: Double): (ScreenObject, Long) => Any = (_, _) => Unit
}