package com.blockworker.base2d.render

import java.nio.charset.StandardCharsets
import java.nio.file.{FileSystems, Files}

import org.lwjgl.opengl.GL20

class Shader(val sourcePath: String, val shaderType: Int) {

  protected var shaderID: Int = -1

  loadShader()

  def loadShader(): Unit = {
    val shaderSource = new String(Files.readAllBytes(FileSystems.getDefault.getPath(sourcePath)), StandardCharsets.UTF_8)
    shaderID = GL20.glCreateShader(shaderType)
    GL20.glShaderSource(shaderID, shaderSource)
    GL20.glCompileShader(shaderID)
    RenderUtils.cancelOnGLError("Shader", "loadShader")
  }

  def getShaderID: Int = shaderID

}
