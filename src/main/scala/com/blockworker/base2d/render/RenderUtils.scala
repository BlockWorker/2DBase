package com.blockworker.base2d.render

import java.io.{BufferedReader, FileInputStream, FileReader, IOException}
import java.nio.ByteBuffer
import java.nio.charset.StandardCharsets
import java.nio.file.{FileSystems, Files}
import java.util.Objects

import com.blockworker.base2d.Main
import com.blockworker.base2d.core.{BaseSettings, InterThread, Resources}
import com.blockworker.base2d.input.MousePosHandler
import de.matthiasmann.twl.utils.PNGDecoder
import de.matthiasmann.twl.utils.PNGDecoder.Format
import org.joml.{Matrix4f, Vector4f}
import org.lwjgl.BufferUtils
import org.lwjgl.opengl.GL11._
import org.lwjgl.opengl.GL13._
import org.lwjgl.opengl._
import org.lwjgl.opengl.GL30._

import scala.collection.mutable

/**
  * Created by Alex on 18.06.2016.
  */
object RenderUtils {

  val loadedTextures = mutable.Map[String, (Int, Int, Int)]()

  var normalVertShader: Shader = _
  var normalFragShader: Shader = _
  var normalProgram: ShaderProgram = _
  var modColor: Vector4f = Color.white
  var shaderProgram: ShaderProgram = _
  var modelMatrix: Matrix4f = _
  var camMatrix: Matrix4f = _
  var screenMatrix: Matrix4f = _
  var defTexId = 0

  val matrix44Buffer = BufferUtils.createFloatBuffer(16)

  /**
    * Gets the GL texture ID, width and height of the specified file, loads the texture if needed or requested.
    * @param path Path of the texture file returned by getTexPath.
    * @param reload If true, texture will be reloaded even if it was loaded before.
    * @return GL texture ID associated with `path`, texture width, texture height
    */
  def getTextureData(path: String, reload: Boolean): (Int, Int, Int) = {
    if (loadedTextures.contains(path) && !reload) {
      loadedTextures(path)
    } else {
      val id = loadTexture(path)
      loadedTextures(path) = id
      id
    }
  }

  /**
    * Same as getTextureData, but only returns the GL texture ID.
    */
  def getTextureId(path: String, reload: Boolean): Int = getTextureData(path, reload)._1

  private def loadTexture(path: String): (Int, Int, Int) = {
    var retBuf: ByteBuffer = null
    var tWidth = 0
    var tHeight = 0
    try {
      val in = new FileInputStream(path)
      val decoder = new PNGDecoder(in)
      tWidth = decoder.getWidth
      tHeight = decoder.getHeight
      retBuf = ByteBuffer.allocateDirect(4 * tWidth * tHeight)
      decoder.decode(retBuf, tWidth * 4, Format.RGBA)
      retBuf.flip()
      in.close()
    } catch {
      case e: IOException =>
        e.printStackTrace()
        System.exit(-1)
    }
    // Create new texture and bind it
    glDisable(GL_TEXTURE_2D)
    val texId = glGenTextures()
    glActiveTexture(GL_TEXTURE0)
    glBindTexture(GL_TEXTURE_2D, texId)
    // All RGB bytes are aligned to each other and each component is 1 byte
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1)
    // Upload texture data and generate mipmaps for scaling
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, tWidth, tHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, retBuf)
    glGenerateMipmap(GL_TEXTURE_2D)
    // Setup UV coord system
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT)
    // Setup what to do when the texture has to be scaled
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR)
    // Unbind texture
    glEnable(GL_TEXTURE_2D)
    bindDefaultTexture()

    cancelOnGLError("loadTexture")

    (texId, tWidth, tHeight)
  }

  def bindDefaultTexture(): Unit = {
    if (!InterThread.isGraphics) {
      InterThread.scheduleGraphics(() => bindDefaultTexture())
      return
    }
    if (!glIsTexture(defTexId)) {
      val buf = ByteBuffer.allocateDirect(4)
      buf.put(Array[Byte](255.toByte, 255.toByte, 255.toByte, 255.toByte))
      buf.flip()
      glDisable(GL_TEXTURE_2D)
      defTexId = glGenTextures()
      glActiveTexture(GL_TEXTURE0)
      glBindTexture(GL_TEXTURE_2D, defTexId)
      glPixelStorei(GL_UNPACK_ALIGNMENT, 1)
      glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 1, 1, 0, GL_RGBA, GL_UNSIGNED_BYTE, buf)
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT)
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT)
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
      glEnable(GL_TEXTURE_2D)
    } else glBindTexture(GL_TEXTURE_2D, defTexId)
  }

  /**
    * Sets `color` as the mod color which is applied to all following renders.
    */
  def setModColor(color: Vector4f): Unit = {
    if (Objects.equals(color, modColor)) return
    modColor = color
    pushModColor()
  }

  def resetModColor(): Unit = setModColor(Color.white)

  private def pushModColor(): Unit = {
    if (shaderProgram == null) return
    val modColorLoc = shaderProgram.getUniformLoc("modColor")
    if (modColorLoc < 0) return
    GL20.glUniform4f(modColorLoc, modColor.x, modColor.y, modColor.z, modColor.w)
    cancelOnGLError("pushModColor")
  }

  def pushMousePos(): Unit = {
    if (shaderProgram == null) return
    val mousePosLoc = shaderProgram.getUniformLoc("mouse")
    if (mousePosLoc < 0) return
    val mousePos = Array[Float](MousePosHandler.mouseX - .5f * BaseSettings.coordRes().width,
                                1 - MousePosHandler.mouseY + .5f * BaseSettings.coordRes().height)
    GL20.glUniform2f(mousePosLoc, mousePos(0), mousePos(1))
    RenderUtils.cancelOnGLError("pushMousePos")
  }

  def useShaderProgram(program: ShaderProgram): Unit = {
    shaderProgram = program
    shaderProgram.useProgram()
    pushFrameMatrices()
    pushModelMatrix()
    pushModColor()
    pushMousePos()
    cancelOnGLError("useShaderProgram")
  }

  def resetShaderProgram(): Unit = useShaderProgram(normalProgram)

  def disableShaderProgram(): Unit = {
    shaderProgram = null
    GL20.glUseProgram(0)
  }

  /**
    * Creates a static texture object from the specified texture file and in the specified UV range.
    * @param path Path of the texture file returned by getTexPath.
    * @param minU Minimum U coordinate.
    * @param maxU Maximum U coordinate.
    * @param minV Minimum V coordinate.
    * @param maxV Maximum V coordinate.
    * @param reload If true, texture will be reloaded even if it was loaded before.
    * @return Ready-to-use texture object.
    */
  def createStaticTexture(path: String, minU: Float, maxU: Float, minV: Float, maxV: Float, reload: Boolean = false): StaticTexture = {
    val texData = getTextureData(path, reload)
    new StaticTexture(path, texData._1, minU, maxU, minV, maxV, texData._2, texData._3)
  }

  /**
    * Creates an animated texture object from the specified texture file and given parameters.
    * @param path Path of the texture file returned by getTexPath.
    * @param frames Number of frames in the animation.
    * @param columns Number of frame columns in the animation texture file.
    * @param rows Number of frame rows in the animation texture file.
    * @param fps Desired animation frames per second.
    * @param reload If true, texture will be reloaded even if it was loaded before.
    * @return Ready-to-use animated texture object.
    */
  def createAnimTexture(path: String, frames: Int, columns: Int, rows: Int, fps: Double, reload: Boolean = false): AnimatedTexture = {
    val texData = getTextureData(path, reload)
    new AnimatedTexture(path, texData._1, frames, columns, rows, fps, texData._2 / columns, texData._3 / rows)
  }

  /**
    * Creates a static texture object from the specified texture file with full UV bounds, which is required for fonts.
    * @param path Path of the texture file returned by getTexPath.
    * @param reload If true, texture will be reloaded even if it was loaded before.
    * @return Ready-to-use animated texture object.
    */
  def createFontTexture(path: String, reload: Boolean = false): StaticTexture = createStaticTexture(path, 0, 1, 0, 1, reload)


  /**
    * Deletes all textures from GL.
    * @note Any textures that are still used in rendering will be reloaded the next time they are rendered.
    */
  def disposeTextures(): Unit = {
    loadedTextures.foreach { o: (String, (Int, Int, Int)) => glDeleteTextures(o._2._1); loadedTextures.remove(o._1) }
    cancelOnGLError("disposeTextures")
  }

  /**
    * Sets up the camera and screen matrices.
    * Model Matrix: Defaults to identity matrix, can be used to offset a rendered object
    * Camera matrix: Defaults to identity matrix, used to move camera around level
    * Screen matrix: Transforms the unit measurements from GL default to another scale:
    *     GL Default:
    *       -1 <= x < 1
    *       -1 <= y < 1
    *       -1 <= z < 1
    *     New scale:
    *       0 <= x < screenWidth
    *       0 <= y < screenHeight
    *       -256 <= z < 256
    * @param ttb Whether the y scale should be reversed to be top-to-bottom.
    */
  def setupMatrices(ttb: Boolean): Unit = {
    modelMatrix = new Matrix4f()
    camMatrix = new Matrix4f()
    screenMatrix = new Matrix4f()
    screenMatrix.m00(2f / BaseSettings.coordRes().width)
    screenMatrix.m11((if (ttb) -2f else 2f) / BaseSettings.coordRes().height)
    screenMatrix.m22(1f / 256f)
    screenMatrix.m30(-1f)
    screenMatrix.m31(if (ttb) 1f else -1f)
  }

  def setupShaders(): Unit = {
    normalVertShader = new Shader(Resources.shader_normalVert, GL20.GL_VERTEX_SHADER)
    normalFragShader = new Shader(Resources.shader_normalFrag, GL20.GL_FRAGMENT_SHADER)

    normalProgram = new ShaderProgram()
    normalProgram.addShader(normalVertShader)
    normalProgram.addShader(normalFragShader)

    normalProgram.addAttrib("in_Position", 0)
    normalProgram.addAttrib("in_Color", 1)
    normalProgram.addAttrib("in_TextureCoord", 2)

    normalProgram.addUniform("screenMat")
    normalProgram.addUniform("cameraMat")
    normalProgram.addUniform("modelMat")
    normalProgram.addUniform("modColor")

    normalProgram.compileProgram()
  }

  def pushFrameMatrices(): Unit = {
    if (shaderProgram == null) return
    val screenMatLoc = shaderProgram.getUniformLoc("screenMat")
    val cameraMatLoc = shaderProgram.getUniformLoc("cameraMat")
    if (screenMatLoc < 0 || cameraMatLoc < 0) return
    screenMatrix.get(matrix44Buffer); matrix44Buffer.flip()
    matrix44Buffer.clear()
    GL20.glUniformMatrix4fv(screenMatLoc, false, matrix44Buffer)
    camMatrix.get(matrix44Buffer); matrix44Buffer.flip()
    matrix44Buffer.clear()
    GL20.glUniformMatrix4fv(cameraMatLoc, false, matrix44Buffer)
    cancelOnGLError("pushFrameMatrices")
  }

  def pushModelMatrix(): Unit = pushModelMatrix(modelMatrix)

  def pushModelMatrix(matrix: Matrix4f): Unit = {
    if (shaderProgram == null) return
    val modelMatLoc = shaderProgram.getUniformLoc("modelMat")
    if (modelMatLoc < 0) return
    matrix.get(matrix44Buffer); matrix44Buffer.flip()
    matrix44Buffer.clear()
    GL20.glUniformMatrix4fv(modelMatLoc, false, matrix44Buffer)
    cancelOnGLError("pushModelMatrix")
  }

  def getGLErrorString(error: Int): String = error match {
    case GL_NO_ERROR => "No error"
    case GL_INVALID_ENUM => "GL_INVALID_ENUM"
    case GL_INVALID_VALUE => "GL_INVALID_VALUE"
    case GL_INVALID_OPERATION => "GL_INVALID_OPERATION"
    case GL_STACK_OVERFLOW => "GL_STACK_OVERFLOW"
    case GL_STACK_UNDERFLOW => "GL_STACK_UNDERFLOW"
    case GL_OUT_OF_MEMORY => "GL_OUT_OF_MEMORY"
    case _ => "Unknown error"
  }

  def cancelOnGLError(`class`: String, method: String): Unit = {
    val err = glGetError()
    if (err != GL_NO_ERROR) Main.error(`class`, "GL Error in method '" + method + "': " + getGLErrorString(err))
  }

  private def cancelOnGLError(method: String): Unit = cancelOnGLError("RenderUtils", method)
}
