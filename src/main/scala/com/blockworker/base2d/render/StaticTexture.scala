package com.blockworker.base2d.render

import com.blockworker.base2d.core.InterThread
import org.lwjgl.opengl.GL11._
import org.lwjgl.opengl.GL13._
/**
  * A static texture object.
  */
class StaticTexture(protected var texLoc: String, protected var texId: Int, protected var minU: Float, protected var maxU: Float,
                    protected var minV: Float, protected var maxV: Float, protected var width: Int, protected var height: Int) extends Texture {

  /**
    * Binds the texture for use in rendering calls.
    * @note This will override any currently bound texture.
    */
  override def bind(): Unit = {
    if (!InterThread.isGraphics) {
      InterThread.scheduleGraphics(() => bind())
      return
    }
    if (!glIsTexture(texId)) {
      reload()
      println("Reloaded Texture " + texLoc)
    }
    glActiveTexture(GL_TEXTURE0)
    glBindTexture(GL_TEXTURE_2D, texId)
  }

  /**
    * Unbinds the texture.
    * @note This will unbind any texture that is currently bound, so watch out!
    */
  override def unbind(): Unit = {
    RenderUtils.bindDefaultTexture()
  }

  override def reload(): Unit = {
    texId = RenderUtils.getTextureId(texLoc, true)
  }

  override def getTexLoc = texLoc
  override def getTexId = texId
  override def getMinU = minU
  override def getMaxU = maxU
  override def getMinV = minV
  override def getMaxV = maxV
  override def getTexWidth = width
  override def getTexHeight = height
  override def updated = false
}
