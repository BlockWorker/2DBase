package com.blockworker.base2d.render

import com.blockworker.base2d.core.InterThread
import com.blockworker.base2d.sprite.ScreenObject
import org.joml.Vector4f
import org.lwjgl.BufferUtils
import org.lwjgl.opengl.GL11._
import org.lwjgl.opengl.GL15._
import org.lwjgl.opengl.GL20._
import org.lwjgl.opengl.GL30._

/**
  * A quad which is filled with a solid color.
  */
class ColorQuad(protected var xPos: Float, protected var yPos: Float, protected var zPos: Float, protected var width: Float,
                protected var height: Float, protected var color: Vector4f) extends ScreenObject {

  private var vaoId: Int = 0
  private var vboId: Int = 0

  // Initially create VAO and VBO data
  createQuad()

  /**
    * Creates a VAO and VBO from the currently set position and size.
    * Called initially and every time new position or size are set.
    */
  def createQuad(): Unit = {
    if (!InterThread.isGraphics) {
      InterThread.scheduleGraphics(() => createQuad())
      return
    }
    val vertices = Array[Float](
      xPos, yPos, zPos, 1, color.x, color.y, color.z, color.w, 0, 0,
      xPos + width, yPos, zPos, 1, color.x, color.y, color.z, color.w, 0, 0,
      xPos + width, yPos + height, zPos, 1, color.x, color.y, color.z, color.w, 0, 0,
      xPos, yPos + height, zPos, 1, color.x, color.y, color.z, color.w, 0, 0
    )
    val vertBuffer = BufferUtils.createFloatBuffer(vertices.length)
    vertBuffer.put(vertices)
    vertBuffer.flip()

    if (vaoId != 0) glDeleteVertexArrays(vaoId)
    if (vboId != 0) glDeleteBuffers(vboId)

    vaoId = glGenVertexArrays()
    glBindVertexArray(vaoId)

    vboId = glGenBuffers()
    glBindBuffer(GL_ARRAY_BUFFER, vboId)
    glBufferData(GL_ARRAY_BUFFER, vertBuffer, GL_STATIC_DRAW)
    glVertexAttribPointer(0, 4, GL_FLOAT, false, 40, 0)
    glVertexAttribPointer(1, 4, GL_FLOAT, false, 40, 16)
    glVertexAttribPointer(2, 2, GL_FLOAT, false, 40, 32)
    glBindBuffer(GL_ARRAY_BUFFER, 0)

    glBindVertexArray(0)
    RenderUtils.cancelOnGLError("ColorQuad", "createQuad")
  }

  /**
    * Renders the quad. Does not set any flags, that has to be done based on context.
    */
  def render(): Unit = {
    if (!InterThread.isGraphics) {
      InterThread.scheduleGraphics(() => render())
      return
    }

    if (!glIsVertexArray(vaoId) || !glIsBuffer(vboId)) createQuad()

    glBindVertexArray(vaoId)
    glEnableVertexAttribArray(0)
    glEnableVertexAttribArray(1)
    glEnableVertexAttribArray(2)

    glDrawArrays(GL_TRIANGLE_FAN, 0, 4)

    glDisableVertexAttribArray(2)
    glDisableVertexAttribArray(1)
    glDisableVertexAttribArray(0)
    glBindVertexArray(0)
    RenderUtils.cancelOnGLError("ColorQuad", "render")
  }

  /**
    * Disables and deletes all GL data associated with this quad.
    */
  def dispose(): Unit = {
    if (!InterThread.isGraphics) {
      InterThread.scheduleGraphics(() => dispose())
      return
    }
    glDisableVertexAttribArray(2)
    glDisableVertexAttribArray(1)
    glDisableVertexAttribArray(0)
    glBindBuffer(GL_ARRAY_BUFFER, 0)
    if (vboId != 0) glDeleteBuffers(vboId)
    glBindVertexArray(0)
    if (vaoId != 0) glDeleteVertexArrays(vaoId)
    RenderUtils.cancelOnGLError("ColorQuad", "dispose")
  }

  // Getter methods
  override def getX = xPos
  override def getY = yPos
  override def getZ = zPos
  override def getWidth = width
  override def getHeight = height
  override def getColor = new Vector4f(color)

  // Setter methods
  override def setPos(x: Float, y: Float, z: Float): Unit = {
    xPos = x
    yPos = y
    zPos = z
    createQuad()
  }

  def setSize(width: Float, height: Float): Unit = {
    this.width = width
    this.height = height
    createQuad()
  }

  /**
    * Using Quad color setters is discouraged, for higher level color changes use
    * [[com.blockworker.base2d.render.RenderUtils#setModColor RenderUtils.setModColor()]] or
    * [[com.blockworker.base2d.render.Color.ColorHelpers#setAsMod (color).setAsMod()]].
    */
  override def setColor(color: Vector4f): Unit = {
    this.color = color
    createQuad()
  }

  override def setWindow(window: Long): Unit = {}

  override def getWindow: Long = 0
}
