package com.blockworker.base2d.render

import org.joml.Vector4f

/**
  * Provides simple color constants and functions.
  */
object Color {

  def white = new Vector4f(1, 1, 1, 1)
  def black = new Vector4f(0, 0, 0, 1)
  def red = new Vector4f(1, 0, 0, 1)
  def green = new Vector4f(0, 1, 0, 1)
  def blue = new Vector4f(0, 0, 1, 1)
  def magenta = new Vector4f(1, 0, 1, 1)
  def yellow = new Vector4f(1, 1, 0, 1)
  def cyan = new Vector4f(0, 1, 1, 1)
  def none = new Vector4f(0, 0, 0, 0)

  def inverse(clr: Vector4f): Vector4f = new Vector4f(1 - clr.x, 1 - clr.y, 1 - clr.z, clr.w)

  def fromRGBA(r: Int, g: Int, b: Int, a: Int) = new Vector4f(
    math.max(0, math.min(255, r)) / 255f,
    math.max(0, math.min(255, g)) / 255f,
    math.max(0, math.min(255, b)) / 255f,
    math.max(0, math.min(255, a)) / 255f
  )

  def fromInt(color: Int) = fromRGBA(
    (color & 0xFF000000) >>> 24,
    (color & 0xFF0000) >>> 16,
    (color & 0xFF00) >>> 8,
    color & 0xFF
  )

  implicit class ColorHelpers(color: Vector4f) {
    /**
      * @return Inverse color.
      */
    def inverse = Color.inverse(color)

    /**
      * Sets this color as the mod color.
      */
    def setAsMod() = RenderUtils.setModColor(color)
  }

}
