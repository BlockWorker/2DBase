package com.blockworker.base2d.sprite

import com.blockworker.base2d.core.updating.UpdateManager
import com.blockworker.base2d.render.{Color, ScreenSpaceRect, Texture, TextureQuad}
import Color.ColorHelpers
import com.blockworker.base2d.Main
import org.joml.Vector4f

/**
  * A sprite which renders a texture.
  */
class TexSprite(override var name: String, protected var tex: Texture, protected var xPos: Float,
                protected var yPos: Float, protected var zPos: Float, protected var color: Vector4f = Color.white,
                protected var window: Long = Main.windows.head, protected var scissor: ScreenSpaceRect = null) extends Sprite {

  protected val quad = new TextureQuad(xPos, yPos, zPos, tex, Color.white)

  override protected def renderSprite(): Unit = {
    color.setAsMod()
    quad.render()
  }

  /**
    * Sets this sprite's position.
    * @param x X coordinate.
    * @param y Y coordinate.
    * @param z Z coordinate.
    */
  override def setPos(x: Float, y: Float, z: Float): Unit = {
    xPos = x
    yPos = y
    zPos = z
    quad.setPos(x, y, z)
  }

  /**
    * Sets this sprite's texture.
    * @param tx New [[com.blockworker.base2d.render.Texture Texture]] object.
    */
  def setTexture(tx: Texture): Unit = {
    tex = tx
    quad.setTexture(tex)
  }
  def getTexture = tex

  override def setColor(clr: Vector4f): Unit = {
    color = clr
  }
  override def getColor = new Vector4f(color)

  override def setScissorBounds(bounds: ScreenSpaceRect): Unit = scissor = bounds

  override def getScissorBounds: ScreenSpaceRect = scissor

  override def setWindow(window: Long): Unit = this.window = window

  override def getWindow: Long = window

  /**
    * Marks this sprite as disposed and deletes GL data.
    */
  override def dispose(): Unit = {
    name += "[DISPOSED]"
    UpdateManager.unregisterFrameUpdate(this)
    quad.dispose()
  }

  override def getX: Float = xPos
  override def getY: Float = yPos
  override def getZ: Float = zPos
  override def getWidth: Float = quad.getWidth
  override def getHeight: Float = quad.getHeight
}
