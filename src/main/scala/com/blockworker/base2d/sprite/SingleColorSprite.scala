package com.blockworker.base2d.sprite

import com.blockworker.base2d.core.updating.UpdateManager
import com.blockworker.base2d.render.{Color, ColorQuad, ScreenSpaceRect}
import Color.ColorHelpers
import com.blockworker.base2d.Main
import org.joml.Vector4f

/**
  * A sprite which renders a solid color.
  */
class SingleColorSprite(override var name: String, protected var color: Vector4f, protected var xPos: Float,
                        protected var yPos: Float, protected var zPos: Float, protected var width: Float,
                        protected var height: Float, protected var window: Long = Main.windows.head,
                        protected var scissor: ScreenSpaceRect = null) extends Sprite {

  protected val quad = new ColorQuad(xPos, yPos, zPos, width, height, Color.white)

  /**
    * @constructor Constructor using separate RGBA values.
    */
  def this(name: String, r: Float, g: Float, b: Float, a: Float, xPos: Float, yPos: Float, zPos: Float, width: Float, height: Float) =
    this(name, new Vector4f(r, g, b, a), xPos, yPos, zPos, width, height)

  override protected def renderSprite(): Unit = {
    color.setAsMod()
    quad.render()
  }

  override def setPos(x: Float, y: Float, z: Float): Unit = {
    xPos = x
    yPos = y
    zPos = z
    quad.setPos(x, y, z)
  }

  def setSize(width: Float, height: Float): Unit = {
    this.width = width
    this.height = height
    quad.setSize(width, height)
  }

  override def setColor(color: Vector4f): Unit = {
    this.color = color
  }

  override def getColor = new Vector4f(color)

  override def setScissorBounds(bounds: ScreenSpaceRect): Unit = scissor = bounds

  override def getScissorBounds: ScreenSpaceRect = scissor

  override def setWindow(window: Long): Unit = this.window = window

  override def getWindow: Long = window

  /**
    * Marks this sprite as disposed and deletes GL data.
    */
  override def dispose(): Unit = {
    name += "[DISPOSED]"
    UpdateManager.unregisterFrameUpdate(this)
    quad.dispose()
  }

  override def getX: Float = xPos
  override def getY: Float = yPos
  override def getZ: Float = zPos
  override def getWidth: Float = width
  override def getHeight: Float = height
}
