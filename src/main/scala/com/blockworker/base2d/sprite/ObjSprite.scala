package com.blockworker.base2d.sprite

import com.blockworker.base2d.Main
import com.blockworker.base2d.core.updating.UpdateManager
import com.blockworker.base2d.render.{Color, ObjModel, ScreenSpaceRect}
import org.joml.{Vector3f, Vector4f}

/**
  * Created by Alex on 12.01.2017.
  */
class ObjSprite(override var name: String, protected var modelLoc: String, protected var scale: Float,
                protected var xPos: Float, protected var yPos: Float, protected var zPos: Float,
                protected var defaultColor: Vector4f = Color.black, protected var window: Long = Main.windows.head,
                protected var scissor: ScreenSpaceRect = null) extends Sprite {

  protected val model = new ObjModel(modelLoc, scale, defaultColor)

  override protected def renderSprite(): Unit = {
    model.drawAll(xPos, yPos, zPos)
  }

  override def setScissorBounds(bounds: ScreenSpaceRect): Unit = scissor = bounds

  override def getScissorBounds: ScreenSpaceRect = scissor

  override def setWindow(window: Long): Unit = this.window = window

  override def getWindow = window

  override def setPos(x: Float, y: Float, z: Float): Unit = {
    xPos = x
    yPos = y
    zPos = z
  }

  override def dispose(): Unit = {
    name += "[DISPOSED]"
    UpdateManager.unregisterFrameUpdate(this)
  }

  override def setColor(clr: Vector4f): Unit = {
    defaultColor = clr
    model.setDefaultColor(defaultColor)
  }
  override def getColor: Vector4f = new Vector4f(defaultColor)

  override def getX: Float = xPos
  override def getY: Float = yPos
  override def getZ: Float = zPos
  override def getWidth: Float = model.getRelXRange._2
  override def getHeight: Float = model.getRelYRange._2
  override def getDepth: Float = model.getRelZRange._2

}
