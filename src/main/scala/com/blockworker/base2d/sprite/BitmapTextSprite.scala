package com.blockworker.base2d.sprite

import com.blockworker.base2d.Main
import com.blockworker.base2d.core.InterThread
import com.blockworker.base2d.core.updating.UpdateManager
import com.blockworker.base2d.input.CharHandler
import com.blockworker.base2d.render._
import org.joml.Vector4f
import org.lwjgl.BufferUtils
import org.lwjgl.opengl.GL11._
import org.lwjgl.opengl.GL15._
import org.lwjgl.opengl.GL20._
import org.lwjgl.opengl.GL30._

/**
  * A sprite which displays a text string using a bitmap font texture. Supports highlighting of text.
  * @note Deprecated because bitmap fonts (BitmapTextSprite) are no longer used. Use TextSprite instead.
  */
@Deprecated
class BitmapTextSprite(override var name: String, protected var text: String, protected var scale: Float, protected var color: Vector4f, protected var fontTex: StaticTexture,
                       protected var xPos: Float, protected var yPos: Float, protected var zPos: Float, protected var highColor: Vector4f = Color.blue,
                       protected var highTextColor: Vector4f = Color.white, protected var highlighted: Range = null,
                       protected var window: Long = Main.windows.head, protected var scissor: ScreenSpaceRect = null) extends Sprite {

  protected val textQuad: TextQuad = new TextQuad(xPos, yPos, zPos, CharHandler.fixText(text), (fontTex.getTexWidth.toFloat / 16f) * scale,
                                                  (fontTex.getTexHeight.toFloat / 16f) * scale, color, highTextColor, highlighted)
  protected val highlight: ColorQuad = new ColorQuad(xPos, yPos, zPos + .001f, 0, getHeight, highColor)

  if (highlighted != null) {
    highlight.setPos(xPos + highlighted.start * textQuad.getCharWidth, yPos, zPos + .001f)
    highlight.setSize(highlighted.length * textQuad.getCharWidth, getHeight)
  }

  override protected def renderSprite(): Unit = {
    if (highlight.getWidth > 0) highlight.render()
    fontTex.bind()
    textQuad.render()
    fontTex.unbind()
  }

  def setText(text: String): Unit = {
    this.text = CharHandler.fixText(text)
    textQuad.setText(this.text)
  }
  def getText = text

  def setScale(scale: Float): Unit = {
    if (scale < 0 || scale > 1) return
    this.scale = scale
    textQuad.setCharSize((fontTex.getTexWidth.toFloat / 16f) * scale, (fontTex.getTexHeight.toFloat / 16f) * scale)
  }
  def getScale = scale

  override def setColor(color: Vector4f): Unit = {
    this.color = color
    textQuad.setColor(color)
  }
  override def getColor = new Vector4f(color)

  def setHighColor(color: Vector4f): Unit = {
    highColor = color
    highlight.setColor(color)
  }
  def getHighColor = highColor

  def setHighTextColor(color: Vector4f): Unit = {
    highTextColor = color
    textQuad.setHighColor(color)
  }
  def getHighTextColor = highTextColor

  def setHighlighted(highl: Range): Unit = {
    highlighted = highl
    if (highlighted != null) {
      highlight.setPos(xPos + highlighted.start * textQuad.getCharWidth, yPos, zPos + .001f)
      highlight.setSize(highlighted.length * textQuad.getCharWidth, getHeight)
    } else highlight.setSize(0, getHeight)
    textQuad.setHighlighted(highlighted)
  }
  def getHighlighted = highlighted

  def setFontTex(tex: StaticTexture): Unit = {
    fontTex = tex
  }
  def getFontTex = fontTex

  override def setPos(x: Float, y: Float, z: Float): Unit = {
    xPos = x
    yPos = y
    zPos = z
    if (highlighted != null) highlight.setPos(xPos + highlighted.start * textQuad.getCharWidth, yPos, zPos + .001f)
    textQuad.setPos(x, y, z)
  }

  override def setScissorBounds(bounds: ScreenSpaceRect): Unit = scissor = bounds

  override def getScissorBounds: ScreenSpaceRect = scissor

  override def setWindow(window: Long): Unit = this.window = window

  override def getWindow: Long = window

  override def dispose(): Unit = {
    name += "[DISPOSED]"
    UpdateManager.unregisterFrameUpdate(this)
    textQuad.dispose()
    highlight.dispose()
  }

  override def getX: Float = xPos
  override def getY: Float = yPos
  override def getZ: Float = zPos
  override def getWidth: Float = textQuad.getWidth
  override def getHeight: Float = textQuad.getCharHeight
  override def getDepth: Float = textQuad.getDepth + .001f + highlight.getDepth
}

/**
  * @note Deprecated because bitmap fonts (BitmapTextSprite) are no longer used. Use TextSprite instead.
  */
@Deprecated
protected class TextQuad(protected var xPos: Float, protected var yPos: Float, protected var zPos: Float,
                         protected var text: Array[Byte], protected var charWidth: Float,
                         protected var charHeight: Float, protected var color: Vector4f, protected var highColor: Vector4f,
                         protected var highlighted: Range) extends ScreenObject {

  def this(x: Float, y: Float, z: Float, text: String, charWidth: Float, charHeight: Float, color: Vector4f, highClr: Vector4f, highl: Range) =
    this(x, y, z, text.toCharArray.map(_.toByte), charWidth, charHeight, color, highClr, highl)

  protected var vaoId: Int = 0
  protected var vboId: Int = 0
  protected var charCount = text.length

  createQuad()

  def createQuad(): Unit = {
    if (!InterThread.isGraphics) {
      InterThread.scheduleGraphics(() => createQuad())
      return
    }
    val vertices = new Array[Float](40 * charCount)
    for (i <- 0 until charCount) {
      val clr = if (highlighted != null && highlighted.contains(i)) highColor else color
      Array[Float](
        xPos + i * charWidth, yPos, zPos, 1, clr.x, clr.y, clr.z, clr.w, (text(i) & 0xF) * .0625f, ((text(i) & 0xF0) >> 4) * .0625f,
        xPos + (i + 1) * charWidth, yPos, zPos, 1, clr.x, clr.y, clr.z, clr.w, ((text(i) & 0xF) + 1) * .0625f, ((text(i) & 0xF0) >> 4) * .0625f,
        xPos + (i + 1) * charWidth, yPos + charHeight, zPos, 1, clr.x, clr.y, clr.z, clr.w, ((text(i) & 0xF) + 1) * .0625f, (((text(i) & 0xF0) >> 4) + 1) * .0625f,
        xPos + i * charWidth, yPos + charHeight, zPos, 1, clr.x, clr.y, clr.z, clr.w, (text(i) & 0xF) * .0625f, (((text(i) & 0xF0) >> 4) + 1) * .0625f
      ).copyToArray(vertices, 40 * i)
    }
    val vertBuffer = BufferUtils.createFloatBuffer(vertices.length)
    vertBuffer.put(vertices)
    vertBuffer.flip()

    if (vaoId != 0) glDeleteVertexArrays(vaoId)
    if (vboId != 0) glDeleteBuffers(vboId)

    vaoId = glGenVertexArrays()
    glBindVertexArray(vaoId)

    vboId = glGenBuffers()
    glBindBuffer(GL_ARRAY_BUFFER, vboId)
    glBufferData(GL_ARRAY_BUFFER, vertBuffer, GL_STATIC_DRAW)
    glVertexAttribPointer(0, 4, GL_FLOAT, false, 40, 0)
    glVertexAttribPointer(1, 4, GL_FLOAT, false, 40, 16)
    glVertexAttribPointer(2, 2, GL_FLOAT, false, 40, 32)
    glBindBuffer(GL_ARRAY_BUFFER, 0)

    glBindVertexArray(0)
    RenderUtils.cancelOnGLError("TextQuad", "createQuad")
  }

  def render(): Unit = {
    if (!glIsVertexArray(vaoId) || !glIsBuffer(vboId)) createQuad()

    glBindVertexArray(vaoId)
    glEnableVertexAttribArray(0)
    glEnableVertexAttribArray(1)
    glEnableVertexAttribArray(2)

    glDrawArrays(GL_QUADS, 0, 4 * charCount)

    glDisableVertexAttribArray(2)
    glDisableVertexAttribArray(1)
    glDisableVertexAttribArray(0)
    glBindVertexArray(0)
    RenderUtils.cancelOnGLError("TextQuad", "render")
  }

  def dispose(): Unit = {
    if (!InterThread.isGraphics) {
      InterThread.scheduleGraphics(() => dispose())
      return
    }
    glDisableVertexAttribArray(2)
    glDisableVertexAttribArray(1)
    glDisableVertexAttribArray(0)
    glBindBuffer(GL_ARRAY_BUFFER, 0)
    if (vboId != 0) glDeleteBuffers(vboId)
    glBindVertexArray(0)
    if (vaoId != 0) glDeleteVertexArrays(vaoId)
    RenderUtils.cancelOnGLError("TextQuad", "dispose")
  }

  // Getter methods
  override def getX = xPos
  override def getY = yPos
  override def getZ = zPos
  override def getWidth = charWidth * charCount
  override def getHeight = getCharHeight
  def getText = new String(text)
  def getCharWidth = charWidth
  def getCharHeight = charHeight
  override def getColor = new Vector4f(color)
  def getHighColor = highColor
  def getHighlighted = highlighted

  // Setter methods
  override def setPos(x: Float, y: Float, z: Float): Unit = {
    xPos = x
    yPos = y
    zPos = z
    createQuad()
  }

  def setText(text: String): Unit = {
    this.text = text.toCharArray.map(_.toByte)
    charCount = this.text.length
    createQuad()
  }

  def setCharSize(width: Float, height: Float): Unit = {
    charWidth = width
    charHeight = height
    createQuad()
  }

  override def setColor(color: Vector4f): Unit = {
    this.color = color
    createQuad()
  }

  def setHighColor(color: Vector4f): Unit = {
    highColor = color
    createQuad()
  }

  def setHighlighted(highl: Range): Unit = {
    highlighted = highl
    createQuad()
  }

  override def setWindow(window: Long): Unit = {}

  override def getWindow: Long = 0
}
