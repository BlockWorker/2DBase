package com.blockworker.base2d.sprite

import org.joml.{Vector3f, Vector4f}

/**
  * Base for all screen based objects.
  */
trait ScreenObject {

  def setPos(x: Float, y: Float, z: Float): Unit

  def setPos(v: Vector3f): Unit = setPos(v.x, v.y, v.z)

  def setPosRelative(dx: Float, dy: Float, dz: Float): Unit = setPos(getX + dx, getY + dy, getZ + dz)

  def setPosRelative(v: Vector3f): Unit = setPosRelative(v.x, v.y, v.z)

  def setColor(color: Vector4f): Unit = {}

  def hide(): Unit = {}

  def show(): Unit = {}

  def setWindow(window: Long)
  def getWindow: Long

  def getX: Float
  def getY: Float
  def getZ: Float
  def getPos: Vector3f = new Vector3f(getX, getY, getZ)
  def getWidth: Float
  def getHeight: Float
  def getDepth: Float = 0
  def getSize: Vector3f = new Vector3f(getWidth, getHeight, getDepth)
  def getColor: Vector4f = null
  def isVisible: Boolean = true
  def getMaxX = getX + getWidth
  def getMaxY = getY + getHeight
  def getMaxZ = getZ + getDepth
  def isInRegion(x: Float, y: Float) = x >= getX && x < getMaxX && y >= getY && y < getMaxY
/*  def overlapsX(obj: ScreenObject): Int = {
    if (getMaxY > obj.getY && getY < obj.getMaxY) {
      if (enclosedIn(obj)) {
        if (getX - obj.getX <= obj.getMaxX - getMaxX) return -1
        else return 1
      }
      if (getMaxX > obj.getX && getX < obj.getX) return -1
      if (getX < obj.getMaxX && getMaxX > obj.getMaxX) return 1
    }
    0
  }
  def overlapsY(obj: ScreenObject): Int = {
    if (getMaxX > obj.getX && getX < obj.getMaxX) {
      if (enclosedIn(obj)) {
        if (getY - obj.getY <= obj.getMaxY - getMaxY) return -1
        else return 1
      }
      if (getMaxY > obj.getY && getY < obj.getY) return -1
      if (getY < obj.getMaxY && getMaxY > obj.getMaxY) return 1
    }
    0
  }*/
  def overlaps(obj: ScreenObject): Boolean =
    getMaxX > obj.getX && getX < obj.getMaxX &&
    getMaxY > obj.getY && getY < obj.getMaxY
/*  def touchesX(obj: ScreenObject): Int = {
    if (getMaxY > obj.getY && getY < obj.getMaxY) {
      if (enclosedIn(obj)) {
        if (getX - obj.getX <= obj.getMaxX - getMaxX) return -1
        else return 1
      }
      if (getMaxX >= obj.getX && getX < obj.getX) return -1
      if (getX <= obj.getMaxX && getMaxX > obj.getMaxX) return 1
    }
    0
  }
  def touchesY(obj: ScreenObject): Int = {
    if (getMaxX > obj.getX && getX < obj.getMaxX) {
      if (enclosedIn(obj)) {
        if (getY - obj.getY <= obj.getMaxY - getMaxY) return -1
        else return 1
      }
      if (getMaxY >= obj.getY && getY < obj.getY) return -1
      if (getY <= obj.getMaxY && getMaxY > obj.getMaxY) return 1
    }
    0
  }
  def touches(obj: ScreenObject): Boolean =
    (getMaxX >= obj.getX && getX <= obj.getMaxX) &&
      (getMaxY >= obj.getY && getY <= obj.getMaxY)
  def enclosedIn(obj: ScreenObject): Boolean =
    getX >= obj.getX && getMaxX <= obj.getMaxX &&
    getY >= obj.getY && getMaxY <= obj.getMaxY*/
}
