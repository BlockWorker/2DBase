package com.blockworker.base2d.sprite

import com.blockworker.base2d.Main
import com.blockworker.base2d.core.InterThread
import com.blockworker.base2d.core.updating.FrameUpdating
import com.blockworker.base2d.render.{RenderUtils, ScreenSpaceRect}
import org.lwjgl.glfw.GLFW
import org.lwjgl.opengl.GL11
/**
  * Created by Alex on 17.06.2016.
  */
trait Sprite extends FrameUpdating {

  var name: String
  var resetColorBeforeRender = true

  /**
    * Called every frame if renderEnabled is true.
    */
  override def render(): Unit = {
    if (!InterThread.isGraphics) {
      InterThread.scheduleGraphics(() => render())
      return
    }
    if (Main.multiWindow) GLFW.glfwMakeContextCurrent(getWindow)
    if (getScissorBounds != null) {
      GL11.glScissor(getScissorBounds.x, getScissorBounds.y, getScissorBounds.width, getScissorBounds.height)
    } else Main.defaultScissor()
    if (resetColorBeforeRender) RenderUtils.resetModColor()
    renderSprite()
    Main.defaultScissor()
    RenderUtils.resetModColor()
    if (Main.multiWindow) GLFW.glfwMakeContextCurrent(Main.windows.head)
  }

  protected def renderSprite(): Unit

  def dispose(): Unit

  def setScissorBounds(bounds: ScreenSpaceRect)
  def getScissorBounds: ScreenSpaceRect

}
