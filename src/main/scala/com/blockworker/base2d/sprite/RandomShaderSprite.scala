package com.blockworker.base2d.sprite

import com.blockworker.base2d.core.{BaseSettings, Resources}
import com.blockworker.base2d.input.MousePosHandler
import com.blockworker.base2d.render.{RenderUtils, Shader, ShaderProgram}
import org.lwjgl.opengl.GL20

class RandomShaderSprite(val _name: String, val _xPos: Float, val _yPos: Float, val _zPos: Float,
                         val _width: Float, val _height: Float)
                         extends SingleColorSprite(_name, 1, 1, 1, 1, _xPos, _yPos, _zPos, _width, _height) {

  val fragShader = new Shader(Resources.getShaderPath(Resources.domain_2DBase, "fun.frag"), GL20.GL_FRAGMENT_SHADER)
  val randShaderProgram = new ShaderProgram()

  randShaderProgram.addShader(RenderUtils.normalVertShader)
  randShaderProgram.addShader(fragShader)

  randShaderProgram.addAttrib("in_Position", 0)
  randShaderProgram.addAttrib("in_Color", 1)
  randShaderProgram.addAttrib("in_TextureCoord", 2)

  randShaderProgram.addUniform("screenMat")
  randShaderProgram.addUniform("cameraMat")
  randShaderProgram.addUniform("modelMat")
  randShaderProgram.addUniform("modColor")
  randShaderProgram.addUniform("mouse")

  randShaderProgram.compileProgram()

  override protected def renderSprite(): Unit = {
    RenderUtils.useShaderProgram(randShaderProgram)
    super.renderSprite()
    RenderUtils.resetShaderProgram()
  }

}
