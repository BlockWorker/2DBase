package com.blockworker.base2d.sprite

import com.blockworker.base2d.core.updating.UpdateManager
import com.blockworker.base2d.render._
import Color.ColorHelpers
import com.blockworker.base2d.Main
import org.joml.{Vector3f, Vector4f}

import scala.collection.mutable

/**
  * Created by Alex on 13.01.2017.
  */

class TextSprite(override var name: String, protected var text: String, protected var scale: Float, protected var color: Vector4f, protected var fontPath: String,
                 protected var xPos: Float, protected var yPos: Float, protected var zPos: Float, protected var highColor: Vector4f = Color.blue,
                 protected var highTextColor: Vector4f = Color.white, protected var highlighted: Range = null,
                 protected var window: Long = Main.windows.head, protected var scissor: ScreenSpaceRect = null) extends Sprite {

  protected var font = new ObjModel(fontPath, scale, Color.white)
  protected var renderInstructions = mutable.Seq[(String, Vector3f)]()
  protected val highlight: ColorQuad = new ColorQuad(xPos, yPos, zPos + .001f, 0, getHeight, highColor)

  createInstructions()

  if (highlighted != null) {
    highlight.setPos(xPos + highlighted.start * getCharWidth, yPos, zPos + .001f)
    highlight.setSize(highlighted.length * getCharWidth, getHeight)
  }

  override protected def renderSprite(): Unit = {
    if (highlight.getWidth > 0) highlight.render()
    color.setAsMod()
    if (highlighted == null || highlighted.isEmpty) font.multiDraw(renderInstructions: _*)
    else {
      font.multiDraw(renderInstructions.take(highlighted.start) ++ renderInstructions.drop(highlighted.end): _*)
      highTextColor.setAsMod()
      //if (highlighted != null && highlighted.nonEmpty)
        font.multiDraw(renderInstructions.slice(highlighted.start, highlighted.end): _*)
    }
  }

  def createInstructions(): Unit = {
    renderInstructions = mutable.Seq[(String, Vector3f)]()
    for (i <- 0 until text.length) {
      val groupName = char2hex(text(i))
      renderInstructions :+= (groupName, new Vector3f(xPos + getCharWidth * i, yPos, zPos))
    }
  }

  private def char2hex(char: Char): String = {
    val base = Integer.toHexString(char).toUpperCase()
    if (base.length == 4) base
    else "000".substring(base.length - 1) + base
  }

  def setText(text: String): Unit = {
    this.text = text.filter(c => c != 0xA && c != 0xD) //do not allow LF or CR
    createInstructions()
  }
  def getText = text

  def setScale(scale: Float): Unit = {
    if (scale <= 0) return
    this.scale = scale
    font.setScale(scale)
  }
  def getScale = scale

  override def setColor(color: Vector4f): Unit = {
    this.color = color
  }
  override def getColor = new Vector4f(color)

  def setHighColor(color: Vector4f): Unit = {
    highColor = color
    highlight.setColor(color)
  }
  def getHighColor = highColor

  def setHighTextColor(color: Vector4f): Unit = {
    highTextColor = color
  }
  def getHighTextColor = highTextColor

  def setHighlighted(highl: Range): Unit = {
    highlighted = highl
    if (highlighted != null) {
      highlight.setPos(xPos + highlighted.start * getCharWidth, yPos, zPos + .001f)
      highlight.setSize(highlighted.length * getCharWidth, getHeight)
    } else highlight.setSize(0, getHeight)

  }
  def getHighlighted = highlighted

  def setFontPath(path: String): Unit = {
    fontPath = path
    font = new ObjModel(path, scale, Color.white)
  }
  def getFontPath = fontPath

  override def setScissorBounds(bounds: ScreenSpaceRect): Unit = scissor = bounds

  override def getScissorBounds: ScreenSpaceRect = scissor

  override def setWindow(window: Long): Unit = this.window = window

  override def getWindow: Long = window

  override def setPos(x: Float, y: Float, z: Float): Unit = {
    xPos = x
    yPos = y
    zPos = z
    if (highlighted != null) highlight.setPos(xPos + highlighted.start * getCharWidth, yPos, zPos + .001f)
    createInstructions()
  }

  override def dispose(): Unit = {
    name += "[DISPOSED]"
    UpdateManager.unregisterFrameUpdate(this)
    font.dispose()
    highlight.dispose()
  }

  def getCharWidth = scale * .5f
  def getCharHeight = scale

  override def getX: Float = xPos
  override def getY: Float = yPos
  override def getZ: Float = zPos
  override def getWidth: Float = getCharWidth * text.length
  override def getHeight: Float = getCharHeight
  override def getDepth: Float = font.getDepth + .001f + highlight.getDepth
}
