package com.blockworker.base2d.sprite

import com.blockworker.base2d.Main
import com.blockworker.base2d.core.updating.UpdateManager
import com.blockworker.base2d.render.{RenderUtils, ScreenSpaceRect}
import org.joml.Vector4f
import org.lwjgl.BufferUtils
import org.lwjgl.opengl.GL11._
import org.lwjgl.opengl.GL15._
import org.lwjgl.opengl.GL20._
import org.lwjgl.opengl.GL30._

/**
  * Created by Alex on 03.01.2017.
  */
abstract class DynamicDrawSprite(override var name: String, protected var xPos: Float, protected var yPos: Float, protected var zPos: Float,
                                 protected var width: Float, protected var height: Float, protected var window: Long = Main.windows.head,
                                 protected var scissor: ScreenSpaceRect = null) extends Sprite {

  private var zOffset = 0f

  override protected def renderSprite(): Unit = {
    zOffset = getDepth
    draw()
  }

  protected def draw(): Unit

  protected def drawTriangle(p1: (Float, Float), p2: (Float, Float), p3: (Float, Float), color: Vector4f): Unit = {
    val vertices = Array[Float](
      xPos + p1._1, yPos + p1._2, zPos + zOffset, 1, color.x, color.y, color.z, color.w, 0, 0,
      xPos + p2._1, yPos + p2._2, zPos + zOffset, 1, color.x, color.y, color.z, color.w, 0, 0,
      xPos + p3._1, yPos + p3._2, zPos + zOffset, 1, color.x, color.y, color.z, color.w, 0, 0
    )
    val vertBuffer = BufferUtils.createFloatBuffer(vertices.length)
    vertBuffer.put(vertices)
    vertBuffer.flip()

    val vaoId = glGenVertexArrays()
    glBindVertexArray(vaoId)

    val vboId = glGenBuffers()
    glBindBuffer(GL_ARRAY_BUFFER, vboId)
    glBufferData(GL_ARRAY_BUFFER, vertBuffer, GL_STATIC_DRAW)
    glVertexAttribPointer(0, 4, GL_FLOAT, false, 40, 0)
    glVertexAttribPointer(1, 4, GL_FLOAT, false, 40, 16)
    glVertexAttribPointer(2, 2, GL_FLOAT, false, 40, 32)
    glBindBuffer(GL_ARRAY_BUFFER, 0)

    glEnableVertexAttribArray(0)
    glEnableVertexAttribArray(1)
    glEnableVertexAttribArray(2)

    glDrawArrays(GL_TRIANGLES, 0, 3)

    glDisableVertexAttribArray(2)
    glDisableVertexAttribArray(1)
    glDisableVertexAttribArray(0)
    glBindVertexArray(0)

    glDeleteBuffers(vboId)
    glDeleteVertexArrays(vaoId)

    zOffset -= .001f

    RenderUtils.cancelOnGLError("DynamicDrawSprite " + name, "drawTriangle")
  }

  protected def drawQuad(p1: (Float, Float), p2: (Float, Float), p3: (Float, Float), p4: (Float, Float), color: Vector4f): Unit = {
    val vertices = Array[Float](
      xPos + p1._1, yPos + p1._2, zPos + zOffset, 1, color.x, color.y, color.z, color.w, 0, 0,
      xPos + p2._1, yPos + p2._2, zPos + zOffset, 1, color.x, color.y, color.z, color.w, 0, 0,
      xPos + p3._1, yPos + p3._2, zPos + zOffset, 1, color.x, color.y, color.z, color.w, 0, 0,
      xPos + p4._1, yPos + p4._2, zPos + zOffset, 1, color.x, color.y, color.z, color.w, 0, 0
    )
    val vertBuffer = BufferUtils.createFloatBuffer(vertices.length)
    vertBuffer.put(vertices)
    vertBuffer.flip()

    val vaoId = glGenVertexArrays()
    glBindVertexArray(vaoId)

    val vboId = glGenBuffers()
    glBindBuffer(GL_ARRAY_BUFFER, vboId)
    glBufferData(GL_ARRAY_BUFFER, vertBuffer, GL_STATIC_DRAW)
    glVertexAttribPointer(0, 4, GL_FLOAT, false, 40, 0)
    glVertexAttribPointer(1, 4, GL_FLOAT, false, 40, 16)
    glVertexAttribPointer(2, 2, GL_FLOAT, false, 40, 32)
    glBindBuffer(GL_ARRAY_BUFFER, 0)

    glEnableVertexAttribArray(0)
    glEnableVertexAttribArray(1)
    glEnableVertexAttribArray(2)

    glDrawArrays(GL_TRIANGLE_FAN, 0, 4)

    glDisableVertexAttribArray(2)
    glDisableVertexAttribArray(1)
    glDisableVertexAttribArray(0)
    glBindVertexArray(0)

    glDeleteBuffers(vboId)
    glDeleteVertexArrays(vaoId)

    zOffset -= .001f

    RenderUtils.cancelOnGLError("DynamicDrawSprite " + name, "drawQuad")
  }

  protected def drawRect(x: Float, y: Float, width: Float, height: Float, color: Vector4f) = drawQuad((x, y), (x + width, y), (x + width, y + height), (x, y + height), color)

  /* TODO: WTF 10 FPS
  protected def drawCircle(m: (Float, Float), r: Float, color: Vector4f): Unit = {
    val vert1 = for (i <- 0d to (Math.PI * 2, Math.PI / (2 * r))) yield Array[Float](
      (xPos + m._1 + Math.cos(i) * r).toFloat,
      (yPos + m._2 + Math.sin(i) * r).toFloat,
      zPos + zOffset, 1, color.x, color.y, color.z, color.w, 0, 0
    )
    var vert2 = List[Float]()
    for (v <- vert1) v.foreach(e => vert2 :+= e)
    val vertices = vert2.toArray
    val vertBuffer = BufferUtils.createFloatBuffer(vertices.length)
    vertBuffer.put(vertices)
    vertBuffer.flip()

    val vaoId = glGenVertexArrays()
    glBindVertexArray(vaoId)

    val vboId = glGenBuffers()
    glBindBuffer(GL_ARRAY_BUFFER, vboId)
    glBufferData(GL_ARRAY_BUFFER, vertBuffer, GL_STATIC_DRAW)
    glVertexAttribPointer(0, 4, GL_FLOAT, false, 40, 0)
    glVertexAttribPointer(1, 4, GL_FLOAT, false, 40, 16)
    glVertexAttribPointer(2, 2, GL_FLOAT, false, 40, 32)
    glBindBuffer(GL_ARRAY_BUFFER, 0)

    glEnableVertexAttribArray(0)
    glEnableVertexAttribArray(1)
    glEnableVertexAttribArray(2)

    glDrawArrays(GL_TRIANGLE_FAN, 0, vertices.length / 10)

    glDisableVertexAttribArray(2)
    glDisableVertexAttribArray(1)
    glDisableVertexAttribArray(0)
    glBindVertexArray(0)

    glDeleteBuffers(vboId)
    glDeleteVertexArrays(vaoId)

    zOffset -= .001f

    RenderUtils.cancelOnGLError("DynamicDrawSprite " + name, "drawCircle")
  }
  */

  override def setPos(x: Float, y: Float, z: Float): Unit = {
    xPos = x
    yPos = y
    zPos = z
  }

  override def dispose(): Unit = {
    name += "[DISPOSED]"
    UpdateManager.unregisterFrameUpdate(this)
  }

  protected def numElements: Int

  override def setScissorBounds(bounds: ScreenSpaceRect): Unit = scissor = bounds

  override def getScissorBounds: ScreenSpaceRect = scissor

  override def setWindow(window: Long): Unit = this.window = window

  override def getWindow: Long = window

  override def getX: Float = xPos
  override def getY: Float = yPos
  override def getZ: Float = zPos
  override def getWidth: Float = width
  override def getHeight: Float = height
  override def getDepth: Float = math.max(.001f * (numElements - 1), 0)
}
