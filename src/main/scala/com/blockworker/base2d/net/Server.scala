package com.blockworker.base2d.net

import java.io.{DataInputStream, DataOutputStream}
import java.net._
import java.util

import com.blockworker.base2d.Main
import com.blockworker.base2d.core.{BaseSettings, Event, InterThread}

/**
  * Created by Alex on 03.06.2017.
  */
class Server(protected var port: Int) {

  protected var connections = List.empty[(Thread, Socket, DataInputStream, DataOutputStream)]
  protected var socket = new ServerSocket(port)
  protected var listenerThread = new Thread(() => listenForConnections())
  protected var acceptConnections = false

  val DataReceived = new Event[(Int, Array[Byte])]
  val NewConnection = new Event[(Int, InetAddress)]
  val ConnectionClosed = new Event[Int]
  val ServerFailed = new Event[Unit]

  listenerThread.start()
  socket.setSoTimeout(1000)

  /**
    * INFINITE LOOP - DO NOT CALL THIS METHOD
    */
  protected def listenForConnections(): Unit = {
    try {
      while (!socket.isClosed && !Main.shouldTerminate) {
        if (acceptConnections) try {
          val conn = socket.accept()
          conn.setSoTimeout(1000)
          val id = connections.length
          val thread = new Thread(() => listen(id))
          connections :+= (thread, conn, new DataInputStream(conn.getInputStream), new DataOutputStream(conn.getOutputStream))
          thread.start()
          InterThread.scheduleLogic(() => NewConnection(this, (id, conn.getInetAddress)))
        } catch {
          case _: Throwable =>
        }
        else Thread.sleep(1000)
      }
    } catch { case e: Throwable => e.printStackTrace() }
    finally { InterThread.scheduleLogic(() => ServerFailed(this, Unit)) }
  }

  def disconnect(id: Int): Unit = {
    if (!connections.isDefinedAt(id)) return
    connections(id)._2.shutdownInput()
    connections(id)._2.shutdownOutput()
    connections(id)._2.close()
    connections = connections.slice(0, id) ++ connections.drop(id + 1)
    InterThread.scheduleLogic(() => ConnectionClosed(this, id))
  }

  def disconnectAll(): Unit = connections.indices.foreach(i => disconnect(i))

  /**
    * INFINITE LOOP - DO NOT CALL THIS METHOD
    */
  def listen(id: Int): Unit = {
    try {
      val conn = connections(id)
      while (conn._2.isConnected && !conn._2.isClosed && !Main.shouldTerminate) try {
        if (conn._3.read() < 0) {
          disconnect(id)
          return
        }
        val header = new Array[Byte](3)
        conn._3.read(header)
        if (!util.Arrays.equals(header, Array[Byte](0x7B, 0x2B, BaseSettings.netHeader()))) throw new Exception
        val length = conn._3.readInt()
        val data = new Array[Byte](length)
        conn._3.read(data)
        InterThread.scheduleLogic(() => DataReceived(this, (id, data)))
      } catch {
        case _: Throwable if conn._2 != null && !conn._2.isClosed => conn._3.skip(conn._3.available())
      }
    } catch {
      case _: SocketException =>
      case e: Throwable => e.printStackTrace()
    } finally { disconnect(id) }
  }

  def send(id: Int, data: Array[Byte], flush: Boolean = true): Unit = {
    val conn = connections(id)
    conn._4.write(Array[Byte](0x01, 0x7B, 0x2B, BaseSettings.netHeader()))
    conn._4.writeInt(data.length)
    conn._4.write(data)
    if (flush) conn._4.flush()
  }

  def broadcastSend(data: Array[Byte], flush: Boolean = true) = connections.indices.foreach(i => send(i, data, flush))

  def close() = socket.close()

  def setAcceptConnections(accept: Boolean): Unit = acceptConnections = accept

  def dispose(): Unit = {
    disconnectAll()
    if (!socket.isClosed) socket.close()
  }

}
