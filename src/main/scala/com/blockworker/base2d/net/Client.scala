package com.blockworker.base2d.net

import java.io.{DataInputStream, DataOutputStream}
import java.net._
import java.util

import com.blockworker.base2d.Main
import com.blockworker.base2d.core.{BaseSettings, Event, InterThread}
/**
  * Created by Alex on 03.06.2017.
  */
class Client(protected var target: InetAddress, port: Int) {

  protected var socket: Socket = _
  protected var inStream: DataInputStream = _
  protected var outStream: DataOutputStream = _
  protected var thread: Thread = _
  var lastException: Exception = _

  val DataReceived = new Event[Array[Byte]]
  val ConnectionClosed = new Event[Unit]

  def connect(): Boolean = {
    disconnect()
    try {
      socket = new Socket(target, port)
      inStream = new DataInputStream(socket.getInputStream)
      outStream = new DataOutputStream(socket.getOutputStream)
      socket.setSoTimeout(1000)
      thread = new Thread(() => listen())
      thread.start()
      return true
    } catch { case e: Exception => lastException = e }
    false
  }

  def disconnect(): Unit = {
    if (socket != null && !socket.isClosed) {
      socket.shutdownInput()
      socket.shutdownOutput()
      socket.close()
      InterThread.scheduleLogic(() => ConnectionClosed(this, Unit))
    }
  }

  /**
    * INFINITE LOOP - DO NOT CALL THIS METHOD
    */
  protected def listen(): Unit = {
    try {
      while (socket.isConnected && !socket.isClosed && !Main.shouldTerminate) try {
        if (inStream.read() < 0) {
          disconnect()
          return
        }
        val header = new Array[Byte](3)
        inStream.read(header)
        if (!util.Arrays.equals(header, Array[Byte](0x7B, 0x2B, BaseSettings.netHeader()))) throw new Exception
        val length = inStream.readInt()
        val data = new Array[Byte](length)
        inStream.read(data)
        InterThread.scheduleLogic(() => DataReceived(this, data))
      } catch {
        case _: Throwable if !socket.isClosed => inStream.skip(inStream.available())
      }
    } catch {
      case _: SocketException =>
      case e: Throwable => e.printStackTrace()
    } finally { disconnect() }
  }

  def send(data: Array[Byte], flush: Boolean = true): Unit = {
    outStream.write(Array[Byte](0x01, 0x7B, 0x2B, BaseSettings.netHeader()))
    outStream.writeInt(data.length)
    outStream.write(data)
    if (flush) outStream.flush()
  }

  def getTargetAddress = target
  def getPort = port
  def getSocket = socket
  def getInStream = inStream
  def getOutStream = outStream
  def isConnected = !socket.isClosed && socket.isConnected

  def dispose(): Unit = disconnect()

}
