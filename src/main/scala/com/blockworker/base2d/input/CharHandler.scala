package com.blockworker.base2d.input

import com.blockworker.base2d.core.{Event, InterThread}
import org.lwjgl.glfw.GLFWCharModsCallback

import scala.collection.mutable

/**
  * Handler for character inputs.
  */
object CharHandler extends GLFWCharModsCallback {

  val OnCharAlways = new Event[(Long, Char)]
  val OnCharUnfocused = new Event[(Long, Char)]

  override def invoke(window: Long, codepoint: Int, mods: Int): Unit = {
    val chars = Character.toChars(codepoint)
    if (Focusable.hasFocus && Focusable.getFocus.charInput) {
      val filteredChars = mutable.IndexedSeq[Char](chars: _*).filter(c => c != 0xA7)
      if (filteredChars.isEmpty) return
      InterThread.scheduleLogic(() => Focusable.getFocus.onChar(filteredChars(0), mods))
    } else chars.foreach(c => OnCharUnfocused(this, (window, c)))
    chars.foreach(c => OnCharAlways(this, (window, c)))
  }

  /**
    * @param str Any string
    * @return `str` with supported unicode symbols after 0xFF converted to their representative font texture location
    * @see [[com.blockworker.base2d.input.CharHandler#fixChar fixChar]]
    *
    * @note Deprecated because bitmap fonts (BitmapTextSprite) are no longer used. Use TextSprite instead.
    */
  @Deprecated
  def fixText(str: String): String = str//new String(for (c <- str.toCharArray.filter(a => a != 0xA7)) yield fixChar(c))

  /*
  /**
    * @param c Input UTF-16 character
    * @return Number representing the position of the character on the font textures, or `0x14` if it doesn't exist on the font texture
    *
    * @note Deprecated because bitmap fonts (BitmapTextSprite) are no longer used. Use TextSprite instead.
    */
  @Deprecated
  def fixChar(c: Char): Char = {
    c match {
      case 0x2510 => 0x2
      case 0x2514 => 0x3
      case 0x2518 => 0x4
      case 0x2502 => 0x5
      case 0x2500 => 0x6
      case 0x2022 => 0x7
      case 0x25D8 => 0x8
      case 0x2642 => 0xB
      case 0x2640 => 0xC
      case 0x266C => 0xE
      case 0x263C => 0xF
      case 0x253C => 0x10
      case 0x25C0 => 0x11
      case 0x2195 => 0x12
      case 0x203C => 0x13
      case 0x2534 => 0x15
      case 0x252C => 0x16
      case 0x2524 => 0x17
      case 0x2191 => 0x18
      case 0x251C => 0x19
      case 0x2192 => 0x1A
      case 0x2190 => 0x1B
      case 0x20AC => 0x80
      case 0x201A => 0x82
      case 0x192 => 0x83
      case 0x201E => 0x84
      case 0x2026 => 0x85
      case 0x2020 => 0x86
      case 0x2021 => 0x87
      case 0x2038 => 0x88
      case 0x2030 => 0x89
      case 0x160 => 0x8A
      case 0x2039 => 0x8B
      case 0x152 => 0x8C
      case 0x17D => 0x8E
      case 0x2018 => 0x91
      case 0x2019 => 0x92
      case 0x201C => 0x93
      case 0x201D => 0x94
      case 0x2027 => 0x95
      case 0x2013 => 0x96
      case 0x2014 => 0x97
      case 0x223C => 0x98
      case 0x2122 => 0x99
      case 0x161 => 0x9A
      case 0x203A => 0x9B
      case 0x153 => 0x9C
      case 0x17E => 0x9E
      case 0x178 => 0x9F
      case l if l <= 0xFF => l
      case _ => 0x14
    }
  }
  */

}
