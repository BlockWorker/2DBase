package com.blockworker.base2d.input

import com.blockworker.base2d.sprite.ScreenObject

/**
  * Defines an object on the screen which reacts to a mouse click.
  */
trait MouseReactions extends ScreenObject {

  /**
    * Called when [[com.blockworker.base2d.input.MouseHandler MouseHandler]] registers a mouse button action within this object's region.
    * @note This is always called on the logic thread, on the next logic update after the action.
    *       The action itself is registered on the graphics thread.
    *
    * @param x X coordinate of the mouse action, relative to xPos
    * @param y Y coordinate of the mouse action, relative to yPos
    * @param button Mouse button that was affected
    * @param action Action that was performed ([[org.lwjgl.glfw.GLFW#GLFW_PRESS GLFW_PRESS]] or [[org.lwjgl.glfw.GLFW#GLFW_RELEASE GLFW_RELEASE]])
    */
  def onMouseButton(x: Float, y: Float, button: Int, action: Int): Unit = {}

  /**
    * Called when [[com.blockworker.base2d.input.MousePosHandler MousePosHandler]] registers the mouse entering this object's region.
    * @note This is always called on the logic thread, on the next logic update after the hover.
    *       The hover itself is registered on the graphics thread.
    */
  def onHoverEnter(): Unit = {}

  /**
    * Called when [[com.blockworker.base2d.input.MousePosHandler MousePosHandler]] registers the mouse leaving this object's region.
    * @note This is always called on the logic thread, on the next logic update after the hover.
    *       The hover itself is registered on the graphics thread.
    */
  def onHoverLeave(): Unit = {}

  /**
    * Called when [[com.blockworker.base2d.input.MousePosHandler MousePosHandler]] registers the mouse moving over this object's region.
    * Only called if this was registered in MousePosHandler with `move = true`.
    * @note This is always called on the logic thread, on the next logic update after the hover.
    *       The hover itself is registered on the graphics thread.
    */
  def onMouseMove(x: Float, y: Float): Unit = {}

  /**
    * Utility method that registers this object in MouseHandler.
    */
  def clickRegister(): Unit = {
    MouseHandler.registerClickable(this)
  }

  def clickUnregister(): Unit = {
    MouseHandler.unregisterClickable(this)
  }

  /**
    * Utility method that registers this object in MousePosHandler.
    */
  def hoverRegister(move: Boolean): Unit = {
    MousePosHandler.registerHoverable(this, move)
  }

  def hoverUnregister(): Unit = {
    MousePosHandler.unregisterHoverable(this)
  }

}
