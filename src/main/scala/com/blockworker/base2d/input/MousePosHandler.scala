package com.blockworker.base2d.input

import com.blockworker.base2d.Main
import com.blockworker.base2d.core.BaseSettings
import com.blockworker.base2d.core.InterThread._
import org.lwjgl.glfw.GLFWCursorPosCallback

import scala.collection.mutable

/**
  * Handler for mouse position changes.
  */
object MousePosHandler extends GLFWCursorPosCallback {

  var mouseX = 0f
  var mouseY = 0f
  val hoverables = mutable.Set[(MouseReactions, Boolean)]()
  var currentHover = Set[(MouseReactions, Boolean)]()

  override def invoke(window: Long, xpos: Double, ypos: Double): Unit = {
    mouseX = (((xpos - Main.letterboxLeft) * BaseSettings.coordRes().width) / Main.letterboxedWidth).toFloat
    mouseY = ((((ypos - Main.letterboxTop) * BaseSettings.coordRes().height * (if (Main.topToBottom) 1d else -1d)) / Main.letterboxedHeight) +
      (if (Main.topToBottom) 0 else BaseSettings.coordRes().height - 1)).toFloat
    val inRegion = hoverables.filter(c => c._1.getWindow == window && c._1.isInRegion(mouseX, mouseY)).toSet
    inRegion.diff(currentHover).foreach(e => scheduleLogic(() => e._1.onHoverEnter()))
    inRegion.filter(e => currentHover.contains(e) && e._2).foreach(e => scheduleLogic(() => e._1.onMouseMove(mouseX - e._1.getX, mouseY - e._1.getY)))
    currentHover.diff(inRegion).foreach(e => scheduleLogic(() => e._1.onHoverLeave()))
    currentHover = inRegion
  }

  def registerHoverable(hoverable: MouseReactions, move: Boolean): Unit = {
    if (!hoverables.exists(m => m._1 == hoverable)) hoverables.add((hoverable, move))
  }

  def unregisterHoverable(hoverable: MouseReactions): Unit = {
    hoverables.retain(m => m._1 != hoverable)
  }

}
