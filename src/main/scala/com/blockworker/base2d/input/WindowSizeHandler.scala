package com.blockworker.base2d.input

import com.blockworker.base2d.Main
import org.lwjgl.glfw.GLFWWindowSizeCallback
import com.blockworker.base2d.core.BaseSettings._
import com.blockworker.base2d.core.GraphicalSize
import org.lwjgl.opengl.GL11

/**
  * Created by Alex on 18.03.2017.
  */
object WindowSizeHandler extends GLFWWindowSizeCallback {

  var handled = false

  override def invoke(window: Long, width: Int, height: Int): Unit = {
    if (window != Main.windows.head || fullscreen() > 0) return
    handled = true
    windowSize.setValue(GraphicalSize(width, height))
    Main.updateLetterbox()
    GL11.glClearColor(1f, 1f, 1f, 1f)
  }

}
