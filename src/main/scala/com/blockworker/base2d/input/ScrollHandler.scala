package com.blockworker.base2d.input

import com.blockworker.base2d.core.InterThread
import org.lwjgl.glfw.GLFWScrollCallback

/**
  * Handler for scroll wheel inputs.
  */
object ScrollHandler extends GLFWScrollCallback {

  /**
    * 0 = scroll down
    * 1 = scroll up
    * 2 = scroll left
    * 3 = scroll right
    */
  val buttons = new Array[Boolean](4)
  var xOffset = 0d
  var yOffset = 0d

  override def invoke(window: Long, xoffset: Double, yoffset: Double): Unit = {

    if (Focusable.hasFocus && Focusable.getFocus.scrollable)
      InterThread.scheduleLogic(() => Focusable.getFocus.onScroll(MousePosHandler.mouseX, MousePosHandler.mouseY, xoffset, yoffset))

    xOffset = xoffset
    yOffset = yoffset
    buttons(0) = yOffset < 0d
    buttons(1) = yOffset > 0d
    buttons(2) = xOffset < 0d
    buttons(3) = xOffset > 0d
  }

}
