package com.blockworker.base2d.input

import com.blockworker.base2d.core.{BaseSettings, Event, InterThread}
import org.lwjgl.glfw.GLFW._
import org.lwjgl.glfw.GLFWKeyCallback

/**
  * Handler for keyboard inputs.
  */
object KeyboardHandler extends GLFWKeyCallback {

  val keys = new Array[Boolean](65536)

  /**
    * Args: window, key, action
    */
  val OnKey = new Event[(Long, Int, Int)]

  override def invoke(window: Long, key: Int, scancode: Int, action: Int, mods: Int): Unit = {
    // Alt+F4 = Close
    if (key == GLFW_KEY_F4 && action == GLFW_PRESS && mods == GLFW_MOD_ALT) glfwSetWindowShouldClose(window, true)

    if (key == GLFW_KEY_Z && action == GLFW_PRESS) {}

    if (Focusable.hasFocus && Focusable.getFocus.keyboardInput) InterThread.scheduleLogic(() => Focusable.getFocus.onKey(key, action, mods))

    keys(key) = action != GLFW_RELEASE

    OnKey(this, (window, key, action))
  }

  def getKey(keycode: Int): Boolean = keys(keycode)

}
