package com.blockworker.base2d.input

object Focusable {

  private var focus: Focusable = _

  def getFocus = focus

  def hasFocus = focus != null

  def setFocus(focusable: Focusable) = {
    if (hasFocus) focus.onLoseFocus()
    focus = focusable
    focus.onGainFocus()
  }

  def removeFocus() = {
    if (hasFocus) focus.onLoseFocus()
    focus = null
  }

}

/**
  * Defines an object which can receive input focus.
  */
trait Focusable {

  var scrollable: Boolean = false
  var keyboardInput: Boolean = false
  var charInput: Boolean = false

  /**
    * Called by [[com.blockworker.base2d.input.ScrollHandler ScrollHandler]] when a scroll input is detected, if scrollable is true.
    * @note This is always called on the logic thread, on the next logic update after the scroll.
    *       The scroll itself is registered on the graphics thread.
    *
    * @param x X screen position of the mouse when the scroll happened.
    * @param y Y screen position of the mouse when the scroll happened.
    * @param xScroll X scroll offset.
    * @param yScroll Y scroll offset.
    */
  def onScroll(x: Float, y: Float, xScroll: Double, yScroll: Double): Unit = {}

  /**
    * Called by [[com.blockworker.base2d.input.KeyboardHandler KeyboardHandler]] when a key input is detected, if keyboardInput is true.
    * @note This is always called on the logic thread, on the next logic update after the key action.
    *       The key action itself is registered on the graphics thread.
    *
    * @see [[org.lwjgl.glfw.GLFWKeyCallback GLFWKeyCallback]]
    */
  def onKey(key: Int, action: Int, mods: Int): Unit = {}

  /**
    * Called by [[com.blockworker.base2d.input.CharHandler CharHandler]] when a character input is detected, if charInput is true.
    * @note This is always called on the logic thread, on the next logic update after the key action.
    *       The char input itself is registered on the graphics thread.
    *
    * @see [[org.lwjgl.glfw.GLFWCharModsCallback GLFWCharModsCallback]]
    */
  def onChar(char: Char, mods: Int): Unit = {}

  /**
    * Called when this object gains focus.
    */
  def onGainFocus(): Unit = {}

  /**
    * Called when this object loses focus.
    */
  def onLoseFocus(): Unit = {}

  def focus(): Unit = {
    Focusable.setFocus(this)
  }

}
