package com.blockworker.base2d.input

import com.blockworker.base2d.core.InterThread
import org.lwjgl.glfw.GLFWMouseButtonCallback
import org.lwjgl.glfw.GLFW._

import scala.collection.mutable

/**
  * Handler for mouse button inputs.
  */
object MouseHandler extends GLFWMouseButtonCallback {

  val buttons = new Array[Boolean](256)
  val clickables = mutable.Set[MouseReactions]()

  override def invoke(window: Long, button: Int, action: Int, mods: Int): Unit = {
    buttons(button) = action != GLFW_RELEASE
    if (action == GLFW_REPEAT) return
    val x = MousePosHandler.mouseX
    val y = MousePosHandler.mouseY
    val inRegion = clickables.filter(c => c.getWindow == window && c.isInRegion(x, y))
    if (inRegion.isEmpty) return
    val clicked = inRegion.minBy(d => d.getZ)
    InterThread.scheduleLogic(() => clicked.onMouseButton(x - clicked.getX, y - clicked.getY, button, action))
  }

  def registerClickable(item: MouseReactions): Unit = {
    if (!clickables.contains(item)) clickables.add(item)
  }

  def unregisterClickable(item: MouseReactions): Unit = {
    if (clickables.contains(item)) clickables.remove(item)
  }

  def getButton(button: Int): Boolean = buttons(button)
}